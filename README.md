# Chess Application Messaging

This is the messaging microservice of the [chess-application].

## Test

Integration tests require jdk8 and docker.

Run test setup (start payara, database, and mock containers and deploy application):
```
./gradlew iTestSetup
```
This only needs to be run before the first test run (not before each).

Run integration tests (redeploys application at start):
```
./gradlew iTest
```

Run test teardown (undeploys application and stops containers):
```
./gradlew iTestTeardown
```
This only needs to be run after the last test run (not after each).

## Build

```
./gradlew war
```

## Expected Environment

The application expects the following variables set in the deployment environment:
1. `CHESSAPP_GAMEHISTORY_URI`
2. `CHESSAPP_MESSAGING_PRIVATEKEY`
3. `CHESSAPP_USER_PUBLICKEY`

Additionally, the path to an sql script may be optionally specified using `CHESSAPP_MESSAGING_SQL`.

## Documentation

Create OpenAPI spec (written to `build/openapi.yaml`):
```
./gradlew resolve
```

## Notes

The test deployment uses postgres while the dev deployment uses embedded derby.

[chess-application]: https://gitlab.com/akluball/chess-app.git
