package com.gitlab.akluball.chessapp.messaging;

import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@DataSourceDefinition(
        name = "java:app/jdbc/messagingDevDs",
        className = "org.apache.derby.jdbc.EmbeddedDataSource",
        databaseName = "messagingDevDb",
        properties = "connectionAttributes=create=true"
)
@Singleton
@Startup
public class MessagingDevDsd {
    @PersistenceContext(unitName = "chessMessagingPu")
    private EntityManager entityManager;

    @PostConstruct
    public void loadSql() {
        String sqlScriptPath = System.getenv("CHESSAPP_MESSAGING_SQL");
        if (Objects.isNull(sqlScriptPath)) {
            return;
        }
        String[] sqlLines;
        try {
            sqlLines = new BufferedReader(new FileReader(new File(sqlScriptPath)))
                    .lines()
                    .toArray(String[]::new);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        List<String> sqlStatements = new ArrayList<>();
        StringBuilder currentStatement = new StringBuilder();
        for (String currentLine : sqlLines) {
            if (currentLine.startsWith("--")) {
                continue;
            }
            currentStatement.append(" ").append(currentLine);
            if (currentStatement.toString().endsWith(";")) {
                sqlStatements.add(currentStatement.substring(0, currentStatement.length() - 1));
                currentStatement = new StringBuilder();
            }
        }
        sqlStatements.forEach((sqlStatement) -> {
            Query nativeQuery = this.entityManager.createNativeQuery(sqlStatement);
            nativeQuery.executeUpdate();
        });
    }
}
