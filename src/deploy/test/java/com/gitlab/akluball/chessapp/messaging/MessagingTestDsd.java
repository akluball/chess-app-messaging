package com.gitlab.akluball.chessapp.messaging;

import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.Singleton;

@Singleton
@DataSourceDefinition(
        name = "java:app/jdbc/messagingTestDs",
        className = "org.postgresql.ds.PGSimpleDataSource",
        serverName = "${ENV=DB_SERVER}",
        databaseName = "${ENV=DB_NAME}",
        user = "${ENV=DB_USER}",
        password = "${ENV=DB_PASSWORD}",
        properties = { "connectionAttributes=;create=true" }
)
public class MessagingTestDsd {
}
