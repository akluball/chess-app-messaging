package com.gitlab.akluball.chessapp.messaging.itest.websocket;

import com.gitlab.akluball.chessapp.messaging.itest.TestConfig;
import com.gitlab.akluball.chessapp.messaging.itest.model.GameConversation;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.WebSocketContainer;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.asRuntime;

public class GameConversationWsManager {

    private final Provider<GameConversationWsClient> gameConversationWsClientProvider;
    private WebSocketContainer websocketContainer;
    private UriBuilder wsUriBuilder;
    private Set<GameConversationWsClient> clients;

    @Inject
    public GameConversationWsManager(TestConfig testConfig,
            Provider<GameConversationWsClient> gameConversationWsClientProvider) {
        this.gameConversationWsClientProvider = gameConversationWsClientProvider;
        this.websocketContainer = ContainerProvider.getWebSocketContainer();
        this.wsUriBuilder = UriBuilder.fromUri(testConfig.messagingUri())
                .scheme("ws")
                .path("api")
                .path("gameconversation")
                .path("{gameHistoryId}");
        this.clients = new HashSet<>();
    }

    public GameConversationWsClient createClient(String token, int gameHistoryId) {
        GameConversationWsClient client = this.gameConversationWsClientProvider.get();
        UriBuilder uriBuilder = this.wsUriBuilder.clone();
        if (token != null) {
            uriBuilder.queryParam("bearer", token);
        }
        URI uri = uriBuilder.resolveTemplate("gameHistoryId", gameHistoryId)
                .build();
        try {
            this.websocketContainer.connectToServer(client, uri);
        } catch (DeploymentException | IOException e) {
            throw asRuntime(e);
        }
        return client;
    }

    public GameConversationWsClient createClient(String token, GameConversation gameConversation) {
        return this.createClient(token, gameConversation.getGameHistoryId());
    }

    public void close() {
        this.clients.forEach(GameConversationWsClient::close);
    }
}
