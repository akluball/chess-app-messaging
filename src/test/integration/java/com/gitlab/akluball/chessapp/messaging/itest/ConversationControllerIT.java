package com.gitlab.akluball.chessapp.messaging.itest;

import com.gitlab.akluball.chessapp.messaging.itest.client.ConversationClient;
import com.gitlab.akluball.chessapp.messaging.itest.data.ConversationDao;
import com.gitlab.akluball.chessapp.messaging.itest.data.ConverserDao;
import com.gitlab.akluball.chessapp.messaging.itest.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;
import com.gitlab.akluball.chessapp.messaging.itest.param.Guicy;
import com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.messaging.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.ConversationDto;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.ConversationInitializerDto;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.messaging.itest.util.Builders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class ConversationControllerIT {
    private final Builders builders;
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final ConverserDao converserDao;
    private final ConversationDao conversationDao;
    private final UserSecurity userSecurity;
    private final ConversationClient conversationClient;

    @Guicy
    ConversationControllerIT(Builders builders,
            UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            ConverserDao converserDao,
            ConversationDao conversationDao,
            UserSecurity userSecurity,
            ConversationClient conversationClient) {
        this.builders = builders;
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.converserDao = converserDao;
        this.conversationDao = conversationDao;
        this.userSecurity = userSecurity;
        this.conversationClient = conversationClient;
    }

    private Converser persistedConverser() {
        Converser converser = this.uniqueBuilders.converser().build();
        this.converserDao.persist(converser);
        return converser;
    }

    @Test
    void create() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).to(recipient).text("hi").build();
        Response response = this.conversationClient.create(this.userSecurity.tokenFor(sender), convInit);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(ConversationDto.class).getId()).isNotNull();
    }

    @Test
    void createNoAuth() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).to(recipient).text("hi").build();
        Response response = this.conversationClient.create(null, convInit);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createBadRole() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).to(recipient).text("hi").build();
        Response response = this.conversationClient.create(this.userSecurity.badRoleTokenFor(sender), convInit);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createBadSignature() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).to(recipient).text("hi").build();
        String token = this.userSecurity.badSignatureTokenFor(sender);
        Response response = this.conversationClient.create(token, convInit);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void createNoConversationInitializerDto() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.conversationClient.create(token, null);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoSenderId() {
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().senderUserId(null).to(recipient)
                .text("hi").build();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.conversationClient.create(token, convInit);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createAlreadyExists() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(sender).converser(recipient).build();
        this.conversationDao.persist(conversation);
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).to(recipient).text("hi").build();
        Response response = this.conversationClient.create(this.userSecurity.tokenFor(sender), convInit);
        assertThat(response.getStatus()).isEqualTo(409);
        assertThat(response.readEntity(String.class)).containsMatch("conflict");
    }

    @Test
    void createCurrentUserNotSender() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).to(recipient).text("hi").build();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.conversationClient.create(token, convInit);
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    void createNoRecipientUserIds() {
        Converser sender = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).text("hi").build();
        Response response = this.conversationClient.create(this.userSecurity.tokenFor(sender), convInit);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createCurrentUserAsRecipient() {
        Converser sender = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).to(sender).text("hi").build();
        Response response = this.conversationClient.create(this.userSecurity.tokenFor(sender), convInit);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createNoMessageText() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).to(recipient).text(null).build();
        Response response = this.conversationClient.create(this.userSecurity.tokenFor(sender), convInit);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void createEmptyMessageText() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto convInit = this.builders.convInitDto().from(sender).to(recipient).text("").build();
        Response response = this.conversationClient.create(this.userSecurity.tokenFor(sender), convInit);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void findById() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converser);
        Response response = this.conversationClient.findByConversationId(token, conversation);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(ConversationDto.class).getId()).isEqualTo(conversation.getId());
    }

    @Test
    void findByIdNoAuth() {
        Conversation conversation = this.builders.conversation().build();
        this.conversationDao.persist(conversation);
        Response response = this.conversationClient.findByConversationId(null, conversation);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByIdBadRole() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser).build();
        this.conversationDao.persist(conversation);
        String badRoleToken = this.userSecurity.badRoleTokenFor(converser);
        Response response = this.conversationClient.findByConversationId(badRoleToken, conversation);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByIdBadSignature() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser).build();
        this.conversationDao.persist(conversation);
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(converser);
        Response response = this.conversationClient.findByConversationId(badSignatureToken, conversation);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByIdExpiredToken() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.expiredTokenFor(converser);
        Response response = this.conversationClient.findByConversationId(token, conversation);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByIdNotAParticipant() {
        Conversation conversation = this.builders.conversation().build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(this.persistedConverser());
        Response response = this.conversationClient.findByConversationId(token, conversation);
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    void findByIdNonIntegerId() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.conversationClient.findByConversationId(token, "not-and-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void findByIdNotFound() {
        Conversation conversation = this.builders.conversation().build();
        String token = this.userSecurity.tokenFor(this.persistedConverser());
        Response response = this.conversationClient.findByConversationId(token, conversation);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    void findByCurrentUser() {
        Converser converser = this.persistedConverser();
        Conversation currentUserIsConverser = this.builders.conversation().converser(converser)
                .message(this.builders.message().epochSeconds(1000L).build()).build();
        Conversation currentUserNotConverser = this.builders.conversation().build();
        this.conversationDao.persist(currentUserIsConverser, currentUserNotConverser);
        Response response = this.conversationClient.findByCurrentUser(this.userSecurity.tokenFor(converser));
        assertThat(response.getStatus()).isEqualTo(200);
        List<Conversation> conversations = response.readEntity(new GenericType<List<Conversation>>() {});
        assertThat(conversations.stream().map(Conversation::getId).collect(Collectors.toList()))
                .containsExactly(currentUserIsConverser.getId());
    }

    @Test
    void findByCurrentUserOrderByLastMessageFreshness() {
        Converser converser = this.persistedConverser();
        Conversation fresh = this.builders.conversation().converser(converser)
                .message(this.builders.message().epochSeconds(3000L).build()).build();
        Conversation staler = this.builders.conversation().converser(converser)
                .message(this.builders.message().epochSeconds(2000L).build()).build();
        Conversation stalest = this.builders.conversation().converser(converser)
                .message(this.builders.message().epochSeconds(1000L).build()).build();
        this.conversationDao.persist(stalest, fresh, staler);
        Response response = this.conversationClient.findByCurrentUser(this.userSecurity.tokenFor(converser));
        assertThat(response.getStatus()).isEqualTo(200);
        List<Conversation> conversations = response.readEntity(new GenericType<List<Conversation>>() {});
        assertThat(conversations.stream().map(Conversation::getId).collect(Collectors.toList()))
                .containsExactly(fresh.getId(), staler.getId(), stalest.getId()).inOrder();
    }

    @Test
    void findByCurrentUserNotPersisted() {
        Converser converser = this.uniqueBuilders.converser().build();
        String token = this.userSecurity.tokenFor(converser);
        Response response = this.conversationClient.findByCurrentUser(token);
        assertThat(response.getStatus()).isEqualTo(200);
        List<Conversation> conversations = response.readEntity(new GenericType<List<Conversation>>() {});
        assertThat(conversations).hasSize(0);
    }

    @Test
    void findByCurrentUserNoAuth() {
        Response response = this.conversationClient.findByCurrentUser(null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByCurrentUserBadRole() {
        Converser converser = this.persistedConverser();
        String badRoleToken = this.userSecurity.badRoleTokenFor(converser);
        Response response = this.conversationClient.findByCurrentUser(badRoleToken);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByCurrentUserBadSignature() {
        Converser converser = this.persistedConverser();
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(converser);
        Response response = this.conversationClient.findByCurrentUser(badSignatureToken);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    private Conversation createPersistedConversation(Converser... conversers) {
        Conversation.Builder conversationBuilder = this.builders.conversation();
        Stream.of(conversers).forEach(conversationBuilder::converser);
        Conversation conversation = conversationBuilder.build();
        this.conversationDao.persist(conversation);
        return conversation;
    }

    @Test
    void findByUserIds() {
        Converser converserOne = this.persistedConverser();
        Converser converserTwo = this.persistedConverser();
        Conversation conversation = createPersistedConversation(converserOne, converserTwo);
        Set<Converser> conversers = Stream.of(converserOne, converserTwo).collect(Collectors.toSet());
        Response response = this.conversationClient.findBetween(this.userSecurity.tokenFor(converserOne), conversers);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(ConversationDto.class).getId()).isEqualTo(conversation.getId());
    }

    @Test
    void findByUserIdsNoUserIds() {
        Set<Converser> conversers = Collections.emptySet();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.conversationClient.findBetween(token, conversers);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void findByUserIdsNoAuth() {
        Converser converserOne = this.persistedConverser();
        Converser converserTwo = this.persistedConverser();
        createPersistedConversation(converserOne, converserTwo);
        Set<Converser> conversers = Stream.of(converserOne, converserTwo).collect(Collectors.toSet());
        Response response = this.conversationClient.findBetween(null, conversers);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByUserIdsBadRole() {
        Converser converserOne = this.persistedConverser();
        Converser converserTwo = this.persistedConverser();
        createPersistedConversation(converserOne, converserTwo);
        Set<Converser> conversers = Stream.of(converserOne, converserTwo).collect(Collectors.toSet());
        String badRoleToken = this.userSecurity.badRoleTokenFor(converserOne);
        Response response = this.conversationClient.findBetween(badRoleToken, conversers);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByUserIdsBadSignature() {
        Converser converserOne = this.persistedConverser();
        Converser converserTwo = this.persistedConverser();
        createPersistedConversation(converserOne, converserTwo);
        Set<Converser> conversers = Stream.of(converserOne, converserTwo).collect(Collectors.toSet());
        String badSignatureToken = this.userSecurity.badRoleTokenFor(converserOne);
        Response response = this.conversationClient.findBetween(badSignatureToken, conversers);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByUserIdsNotParticipant() {
        Converser converserOne = this.persistedConverser();
        Converser converserTwo = this.persistedConverser();
        createPersistedConversation(converserOne, converserTwo);
        Set<Converser> conversers = Stream.of(converserOne, converserTwo).collect(Collectors.toSet());
        String token = this.userSecurity.tokenFor(this.persistedConverser());
        Response response = this.conversationClient.findBetween(token, conversers);
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    void findByUserIdsNotFound() {
        Converser converserOne = this.persistedConverser();
        Converser converserTwo = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converserOne).build();
        this.conversationDao.persist(conversation);
        Set<Converser> conversers = Stream.of(converserOne, converserTwo).collect(Collectors.toSet());
        Response response = this.conversationClient.findBetween(this.userSecurity.tokenFor(converserOne), conversers);
        assertThat(response.getStatus()).isEqualTo(404);
    }
}
