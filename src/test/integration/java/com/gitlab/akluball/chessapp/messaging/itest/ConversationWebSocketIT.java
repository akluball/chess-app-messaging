package com.gitlab.akluball.chessapp.messaging.itest;

import com.gitlab.akluball.chessapp.messaging.itest.data.ConversationDao;
import com.gitlab.akluball.chessapp.messaging.itest.data.ConverserDao;
import com.gitlab.akluball.chessapp.messaging.itest.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;
import com.gitlab.akluball.chessapp.messaging.itest.param.Guicy;
import com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.messaging.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.messaging.itest.util.Builders;
import com.gitlab.akluball.chessapp.messaging.itest.websocket.ConversationWsClient;
import com.gitlab.akluball.chessapp.messaging.itest.websocket.ConversationWsManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import java.util.stream.Stream;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class ConversationWebSocketIT {
    private final Builders builders;
    private final UniqueBuilders uniqueBuilders;
    private final ConverserDao converserDao;
    private final ConversationDao conversationDao;
    private final UserSecurity userSecurity;
    private final ConversationWsManager conversationWsManager;

    @Guicy
    ConversationWebSocketIT(Builders builders,
            UniqueBuilders uniqueBuilders,
            ConverserDao converserDao,
            ConversationDao conversationDao,
            UserSecurity userSecurity,
            ConversationWsManager conversationWsManager) {
        this.builders = builders;
        this.uniqueBuilders = uniqueBuilders;
        this.converserDao = converserDao;
        this.conversationDao = conversationDao;
        this.userSecurity = userSecurity;
        this.conversationWsManager = conversationWsManager;
    }

    @AfterEach
    void cleanup() {
        this.conversationWsManager.close();
    }

    private Converser persistedConverser() {
        Converser converser = this.uniqueBuilders.converser().build();
        this.converserDao.persist(converser);
        return converser;
    }

    private Conversation createPersistedConversation(Converser... conversers) {
        Conversation.Builder conversationBuilder = this.builders.conversation();
        Stream.of(conversers).forEach(conversationBuilder::converser);
        Conversation conversation = conversationBuilder.build();
        this.conversationDao.persist(conversation);
        return conversation;
    }

    @Test
    void sendMessageBroadcasts() {
        Converser converserOne = this.persistedConverser();
        Converser converserTwo = this.persistedConverser();
        Conversation conversation = this.createPersistedConversation(converserOne, converserTwo);
        String tokenOne = this.userSecurity.tokenFor(converserOne);
        String tokenTwo = this.userSecurity.tokenFor(converserTwo);
        ConversationWsClient wsClientOne = this.conversationWsManager.createClient(tokenOne, conversation);
        ConversationWsClient wsClientTwo = this.conversationWsManager.createClient(tokenTwo, conversation);
        wsClientOne.sendMessage("sent message text to broadcast");
        assertThat(wsClientOne.nextMessage().getText()).isEqualTo("sent message text to broadcast");
        assertThat(wsClientTwo.nextMessage().getText()).isEqualTo("sent message text to broadcast");
    }

    @Test
    void emptyText() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.createPersistedConversation(converser);
        String token = this.userSecurity.tokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(token, conversation);
        wsClient.sendMessage("");
        assertThat(wsClient.nextMessage()).isNull();
    }

    @Test
    void noAuth() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.createPersistedConversation(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(null, conversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void badRole() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.createPersistedConversation(converser);
        String token = this.userSecurity.badRoleTokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(token, conversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void badSignature() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.createPersistedConversation(converser);
        String token = this.userSecurity.badSignatureTokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(token, conversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void expiredToken() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.createPersistedConversation(converser);
        String expiredToken = this.userSecurity.expiredTokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(expiredToken, conversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void conversationNotFound() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.createPersistedConversation(converser);
        this.conversationDao.delete(conversation);
        String token = this.userSecurity.tokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(token, conversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("not found");
    }

    @Test
    void converserNotParticipant() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.createPersistedConversation();
        String token = this.userSecurity.tokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(token, conversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("not authorized");
    }
}
