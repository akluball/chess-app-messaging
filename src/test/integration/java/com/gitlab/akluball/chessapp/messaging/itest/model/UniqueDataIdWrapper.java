package com.gitlab.akluball.chessapp.messaging.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Wraps a generated value used for creating unique data across threads
 */
@Entity
@Access(AccessType.FIELD)
public class UniqueDataIdWrapper {
    @Id
    @GeneratedValue
    private int id;

    public int getId() {
        return this.id;
    }
}