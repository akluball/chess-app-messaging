package com.gitlab.akluball.chessapp.messaging.itest.websocket;

import com.gitlab.akluball.chessapp.messaging.itest.transfer.MessageDto;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.asRuntime;
import static com.gitlab.akluball.chessapp.messaging.itest.websocket.WebSocketUtil.WAIT_MILLIS;

@ClientEndpoint(
        decoders = { MessageDtoDecoder.class }
)
public class ConversationWsClient {
    private Session session;
    private RemoteEndpoint.Basic basicRemote;
    private BlockingQueue<MessageDto> receivedMessages = new ArrayBlockingQueue<>(100);
    private BlockingQueue<CloseReason> closeReasons = new ArrayBlockingQueue<>(1);

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        this.session = session;
        this.basicRemote = session.getBasicRemote();
    }

    @OnClose
    public void onClose(CloseReason closeReason) {
        this.closeReasons.add(closeReason);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        throw asRuntime(throwable);
    }

    @OnMessage
    public void onMessage(Session session, MessageDto message) {
        this.receivedMessages.add(message);
    }

    public void sendMessage(String messageText) {
        try {
            this.basicRemote.sendObject(messageText);
        } catch (IOException | EncodeException e) {
            throw asRuntime(e);
        }
    }

    public MessageDto nextMessage() {
        try {
            return this.receivedMessages.poll(WAIT_MILLIS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw asRuntime(e);
        }
    }

    public CloseReason waitForCloseReason() {
        try {
            return this.closeReasons.poll(WAIT_MILLIS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw asRuntime(e);
        }
    }

    public void close() {
        try {
            this.session.close();
        } catch (IOException e) {
            throw asRuntime(e);
        }
    }
}
