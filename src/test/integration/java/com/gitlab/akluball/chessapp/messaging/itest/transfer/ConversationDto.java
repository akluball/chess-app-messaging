package com.gitlab.akluball.chessapp.messaging.itest.transfer;

import java.util.List;
import java.util.Set;

public class ConversationDto {
    private Integer id;
    private Set<Integer> converserIds;
    private List<MessageDto> messages;

    public Integer getId() {
        return this.id;
    }

    public Set<Integer> getConverserIds() {
        return converserIds;
    }

    public List<MessageDto> getMessages() {
        return messages;
    }
}
