package com.gitlab.akluball.chessapp.messaging.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Access(AccessType.FIELD)
public class Conversation {
    @Id
    @GeneratedValue
    private int id;
    @ManyToMany(mappedBy = "conversations")
    private Set<Converser> conversers = new HashSet<>();
    @ElementCollection
    @OrderColumn
    private List<Message> messages = new ArrayList<>();

    public int getId() {
        return id;
    }

    public Set<Converser> getConversers() {
        return conversers;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public Message getLastMessage() {
        return (this.messages.isEmpty())
                ? null
                : this.messages.get(this.messages.size() - 1);
    }

    public static class Builder {
        private final Conversation conversation;

        Builder() {
            this.conversation = new Conversation();
        }

        public Builder converser(Converser converser) {
            this.conversation.conversers.add(converser);
            converser.participantIn(this.conversation);
            return this;
        }

        public Builder message(Message message) {
            this.conversation.messages.add(message);
            return this;
        }

        public Conversation build() {
            return this.conversation;
        }
    }
}
