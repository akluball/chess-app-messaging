package com.gitlab.akluball.chessapp.messaging.itest.transfer;

import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;

import java.util.HashSet;
import java.util.Set;

public class ConversationInitializerDto {
    private Integer senderUserId;
    private Set<Integer> recipientUserIds = new HashSet<>();
    private String messageText;

    public static class Builder {
        private final ConversationInitializerDto conversationInitializerDto;

        Builder() {
            this.conversationInitializerDto = new ConversationInitializerDto();
        }

        public Builder senderUserId(Integer userId) {
            this.conversationInitializerDto.senderUserId = userId;
            return this;
        }

        public Builder from(Converser sender) {
            return this.senderUserId(sender.getUserId());
        }

        public Builder to(Converser recipient) {
            this.conversationInitializerDto.recipientUserIds.add(recipient.getUserId());
            return this;
        }

        public Builder text(String messageText) {
            this.conversationInitializerDto.messageText = messageText;
            return this;
        }

        public ConversationInitializerDto build() {
            return this.conversationInitializerDto;
        }
    }
}
