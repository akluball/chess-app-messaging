package com.gitlab.akluball.chessapp.messaging.itest;

import com.gitlab.akluball.chessapp.messaging.itest.data.GameConversationDao;
import com.gitlab.akluball.chessapp.messaging.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.messaging.itest.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.itest.param.Guicy;
import com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.messaging.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.messaging.itest.websocket.GameConversationWsClient;
import com.gitlab.akluball.chessapp.messaging.itest.websocket.GameConversationWsManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.websocket.CloseReason;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameConversationWebSocketIT {
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final GameConversationDao gameConversationDao;
    private final UserSecurity userSecurity;
    private final GameConversationWsManager gameConversationWsManager;

    @Guicy
    GameConversationWebSocketIT(UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            GameConversationDao gameConversationDao,
            UserSecurity userSecurity,
            GameConversationWsManager gameConversationWsManager) {
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.gameConversationDao = gameConversationDao;
        this.userSecurity = userSecurity;
        this.gameConversationWsManager = gameConversationWsManager;
    }

    @AfterEach
    void cleanup() {
        this.gameConversationWsManager.close();
    }

    private GameConversation persistedGameConversation() {
        GameConversation gameConversation = this.uniqueBuilders.gameConversation().build();
        this.gameConversationDao.persist(gameConversation);
        return gameConversation;
    }

    @Test
    void sendBroadcasts() {
        GameConversation gameConversation = this.persistedGameConversation();
        String tokenOne = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        String tokenTwo = this.userSecurity.tokenFor(gameConversation.getBlackSideUserId());
        GameConversationWsClient wsClientOne = this.gameConversationWsManager.createClient(tokenOne, gameConversation);
        GameConversationWsClient wsClientTwo = this.gameConversationWsManager.createClient(tokenTwo, gameConversation);
        wsClientOne.sendMessage("sent message text to broadcast");
        assertThat(wsClientOne.nextMessage().getText()).isEqualTo("sent message text to broadcast");
        assertThat(wsClientTwo.nextMessage().getText()).isEqualTo("sent message text to broadcast");
    }

    @Test
    void sendEmpty() {
        GameConversation gameConversation = this.persistedGameConversation();
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(token, gameConversation);
        wsClient.sendMessage("");
        assertThat(wsClient.nextMessage()).isNull();
    }

    @Test
    void noAuth() {
        GameConversation gameConversation = this.persistedGameConversation();
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(null, gameConversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseReason.CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void badRole() {
        GameConversation gameConversation = this.persistedGameConversation();
        String badRoleToken = this.userSecurity.badRoleTokenFor(gameConversation.getBlackSideUserId());
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(badRoleToken, gameConversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseReason.CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void badSignature() {
        GameConversation gameConversation = this.persistedGameConversation();
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(gameConversation.getBlackSideUserId());
        GameConversationWsClient wsClient = this.gameConversationWsManager
                .createClient(badSignatureToken, gameConversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseReason.CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    void expiredToken() {
        GameConversation gameConversation = this.persistedGameConversation();
        String expiredToken = this.userSecurity.expiredTokenFor(gameConversation.getBlackSideUserId());
        GameConversationWsClient wsClient = this.gameConversationWsManager
                .createClient(expiredToken, gameConversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseReason.CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("invalid credentials");
    }

    @Test
    @Guicy
    void gameConversationNotFound(MockGameHistory mockGameHistory) {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        mockGameHistory.expectGetNotFound(gameHistoryId);
        String token = userSecurity.tokenFor(this.uniqueData.userId());
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(token, gameHistoryId);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseReason.CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("not found");
        mockGameHistory.clearGet(gameHistoryId);
    }

    @Test
    void gameConversationNotParticipant() {
        GameConversation gameConversation = this.persistedGameConversation();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(token, gameConversation);
        CloseReason closeReason = wsClient.waitForCloseReason();
        assertThat(closeReason.getCloseCode()).isEqualTo(CloseReason.CloseCodes.UNEXPECTED_CONDITION);
        assertThat(closeReason.getReasonPhrase()).containsMatch("not authorized");
    }
}
