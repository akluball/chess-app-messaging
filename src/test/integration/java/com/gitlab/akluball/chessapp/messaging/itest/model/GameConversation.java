package com.gitlab.akluball.chessapp.messaging.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OrderColumn;
import java.util.ArrayList;
import java.util.List;

@Entity
@Access(AccessType.FIELD)
public class GameConversation {
    @Id
    private int gameHistoryId;
    private int whiteSideUserId;
    private int blackSideUserId;
    @ElementCollection
    @OrderColumn
    private List<Message> messages = new ArrayList<>();

    public int getGameHistoryId() {
        return gameHistoryId;
    }

    public int getWhiteSideUserId() {
        return whiteSideUserId;
    }

    public int getBlackSideUserId() {
        return blackSideUserId;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public Message getLastMessage() {
        return (this.messages.isEmpty()) ? null : this.messages.get(this.messages.size() - 1);
    }

    public static class Builder {
        private final GameConversation gameConversation;

        Builder() {
            this.gameConversation = new GameConversation();
        }

        public Builder gameHistoryId(int gameHistoryId) {
            this.gameConversation.gameHistoryId = gameHistoryId;
            return this;
        }

        public Builder whiteSideUserId(int whiteSideUserId) {
            this.gameConversation.whiteSideUserId = whiteSideUserId;
            return this;
        }

        public Builder blackSideUserId(int blackSideUserId) {
            this.gameConversation.blackSideUserId = blackSideUserId;
            return this;
        }

        public Builder message(Message message) {
            this.gameConversation.messages.add(message);
            return this;
        }

        public GameConversation build() {
            return this.gameConversation;
        }
    }
}
