package com.gitlab.akluball.chessapp.messaging.itest.util;

import com.gitlab.akluball.chessapp.messaging.itest.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;
import com.gitlab.akluball.chessapp.messaging.itest.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.itest.model.Message;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.ConversationInitializerDto;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.GameHistoryDto;
import com.google.inject.Inject;

import javax.inject.Provider;

public class Builders {
    private final Provider<Converser.Builder> converserBuilderProvider;
    private final Provider<Conversation.Builder> conversationBuilderProvider;
    private final Provider<ConversationInitializerDto.Builder> convInitDtoBuilderProvider;
    private final Provider<Message.Builder> messageBuilderProvider;
    private final Provider<GameConversation.Builder> gameConversationBuilderProvider;
    private final Provider<GameHistoryDto.Builder> gameHistoryDtoBuilderProvider;

    @Inject
    Builders(Provider<Converser.Builder> converserBuilderProvider,
            Provider<Conversation.Builder> conversationBuilderProvider,
            Provider<ConversationInitializerDto.Builder> convInitDtoBuilderProvider,
            Provider<Message.Builder> messageBuilderProvider,
            Provider<GameConversation.Builder> gameConversationBuilderProvider,
            Provider<GameHistoryDto.Builder> gameHistoryDtoBuilderProvider) {
        this.converserBuilderProvider = converserBuilderProvider;
        this.conversationBuilderProvider = conversationBuilderProvider;
        this.convInitDtoBuilderProvider = convInitDtoBuilderProvider;
        this.messageBuilderProvider = messageBuilderProvider;
        this.gameConversationBuilderProvider = gameConversationBuilderProvider;
        this.gameHistoryDtoBuilderProvider = gameHistoryDtoBuilderProvider;
    }

    public Converser.Builder converser() {
        return this.converserBuilderProvider.get();
    }

    public Conversation.Builder conversation() {
        return this.conversationBuilderProvider.get();
    }

    public ConversationInitializerDto.Builder convInitDto() {
        return this.convInitDtoBuilderProvider.get();
    }

    public Message.Builder message() {
        return this.messageBuilderProvider.get();
    }

    public GameConversation.Builder gameConversation() {
        return this.gameConversationBuilderProvider.get();
    }

    public GameHistoryDto.Builder gameHistoryDto() {
        return this.gameHistoryDtoBuilderProvider.get();
    }
}
