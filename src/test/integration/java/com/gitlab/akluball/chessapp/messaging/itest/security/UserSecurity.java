package com.gitlab.akluball.chessapp.messaging.itest.security;

import com.gitlab.akluball.chessapp.messaging.itest.TestConfig;
import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.Date;
import java.time.Instant;
import java.util.Collections;

import static com.gitlab.akluball.chessapp.messaging.itest.security.SecurityUtil.rsaPrivateKey;
import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.asRuntime;

@Singleton
public class UserSecurity {
    private final RSASSASigner signer;
    private final RSASSASigner badSigner;

    @Inject
    UserSecurity(TestConfig testConfig) {
        this.signer = new RSASSASigner(testConfig.userPrivateKey());
        this.badSigner = new RSASSASigner(rsaPrivateKey());
    }

    private String createToken(int userId, String role, JWSSigner signer, Instant expiration) {
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject("" + userId)
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(role))
                .expirationTime(Date.from(expiration))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        try {
            token.sign(signer);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        return token.serialize();
    }

    public String tokenFor(int userId) {
        return this.createToken(userId, BearerRole.USER.asString(), this.signer, Instant.now().plusSeconds(60));
    }

    public String tokenFor(Converser converser) {
        return this.createToken(converser.getUserId(), BearerRole.USER.asString(), this.signer,
                Instant.now().plusSeconds(60));
    }

    public String badRoleTokenFor(int userId) {
        return this.createToken(userId, "bad-role", this.signer, Instant.now().plusSeconds(60));
    }

    public String badRoleTokenFor(Converser converser) {
        return this.createToken(converser.getUserId(), "bad-role", this.signer, Instant.now().plusSeconds(60));
    }

    public String badSignatureTokenFor(int userId) {
        return this.createToken(userId, BearerRole.USER.asString(), this.badSigner, Instant.now().plusSeconds(60));
    }

    public String badSignatureTokenFor(Converser converser) {
        return this.createToken(converser.getUserId(), BearerRole.USER.asString(), this.badSigner,
                Instant.now().plusSeconds(60));
    }

    public String expiredTokenFor(int userId) {
        Instant expiration = Instant.now().minusSeconds(60);
        return this.createToken(userId, BearerRole.USER.asString(), this.signer, expiration);
    }

    public String expiredTokenFor(Converser converser) {
        Instant expiration = Instant.now().minusSeconds(60);
        return this.createToken(converser.getUserId(), BearerRole.USER.asString(), this.signer, expiration);
    }
}
