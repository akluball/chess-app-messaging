package com.gitlab.akluball.chessapp.messaging.itest.websocket;

import com.gitlab.akluball.chessapp.messaging.itest.TestConfig;
import com.gitlab.akluball.chessapp.messaging.itest.model.Conversation;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.WebSocketContainer;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.asRuntime;

public class ConversationWsManager {

    private final Provider<ConversationWsClient> conversationWsClientProvider;
    private WebSocketContainer websocketContainer;
    private UriBuilder uriBuilder;
    private Set<ConversationWsClient> clients;

    @Inject
    public ConversationWsManager(TestConfig testConfig,
            Provider<ConversationWsClient> conversationWsClientProvider) {
        this.conversationWsClientProvider = conversationWsClientProvider;
        this.websocketContainer = ContainerProvider.getWebSocketContainer();
        this.uriBuilder = UriBuilder.fromUri(testConfig.messagingUri())
                .scheme("ws")
                .path("api")
                .path("conversation")
                .path("{conversationId}");
        this.clients = new HashSet<>();
    }

    public ConversationWsClient createClient(String token, Conversation conversation) {
        ConversationWsClient client = this.conversationWsClientProvider.get();
        UriBuilder uriBuilder = this.uriBuilder.clone();
        if (token != null) {
            uriBuilder.queryParam("bearer", token);
        }
        URI uri = uriBuilder.resolveTemplate("conversationId", conversation.getId()).build();
        try {
            this.websocketContainer.connectToServer(client, uri);
        } catch (DeploymentException | IOException e) {
            throw asRuntime(e);
        }
        this.clients.add(client);
        return client;
    }

    public void close() {
        this.clients.forEach(ConversationWsClient::close);
    }
}
