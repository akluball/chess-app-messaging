package com.gitlab.akluball.chessapp.messaging.itest.transfer;

public class GameHistoryDto {
    private int id;
    private int whiteSideUserId;
    private int blackSideUserId;

    public int getId() {
        return id;
    }

    public int getWhiteSideUserId() {
        return whiteSideUserId;
    }

    public int getBlackSideUserId() {
        return blackSideUserId;
    }

    public static class Builder {
        private final GameHistoryDto gameHistoryDto;

        Builder() {
            this.gameHistoryDto = new GameHistoryDto();
        }

        public Builder id(int id) {
            this.gameHistoryDto.id = id;
            return this;
        }

        public Builder whiteSideUserId(int whiteSideUserId) {
            this.gameHistoryDto.whiteSideUserId = whiteSideUserId;
            return this;
        }

        public Builder blackSideUserId(int blackSideUserId) {
            this.gameHistoryDto.blackSideUserId = blackSideUserId;
            return this;
        }

        public GameHistoryDto build() {
            return this.gameHistoryDto;
        }
    }
}
