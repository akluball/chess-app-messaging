package com.gitlab.akluball.chessapp.messaging.itest.client;

import com.gitlab.akluball.chessapp.messaging.itest.TestConfig;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

public class ApiWrapper {
    private final WebTarget target;

    @Inject
    ApiWrapper(TestConfig testConfig, Client client) {
        this.target = client.target(String.format("%s/api", testConfig.messagingUri()));
    }

    public WebTarget target(String path) {
        return this.target.path(path);
    }
}
