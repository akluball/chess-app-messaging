package com.gitlab.akluball.chessapp.messaging.itest.websocket;

import com.gitlab.akluball.chessapp.messaging.itest.transfer.MessageDto;

import javax.json.bind.Jsonb;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import static com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver.INJECTOR;

public class MessageDtoDecoder implements Decoder.Text<MessageDto> {
    private final Jsonb jsonb = INJECTOR.getInstance(Jsonb.class);

    @Override
    public MessageDto decode(String encodedMessage) {
        return this.jsonb.fromJson(encodedMessage, MessageDto.class);
    }

    @Override
    public boolean willDecode(String encodedMessage) {
        return true;
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
