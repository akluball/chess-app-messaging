package com.gitlab.akluball.chessapp.messaging.itest.unique;

import com.gitlab.akluball.chessapp.messaging.itest.data.UniqueDataDao;

import javax.inject.Inject;
import java.util.concurrent.atomic.AtomicInteger;

public class UniqueData {
    private final int id;
    private final AtomicInteger userIdCounter;
    private final AtomicInteger gameHistoryIdCounter;

    @Inject
    UniqueData(UniqueDataDao uniqueDataDao) {
        this.id = uniqueDataDao.nextId();
        int start = id * 1_000;
        this.userIdCounter = new AtomicInteger(start);
        this.gameHistoryIdCounter = new AtomicInteger(start);
    }

    public int userId() {
        return this.userIdCounter.getAndIncrement();
    }

    public int gameHistoryId() {
        return this.gameHistoryIdCounter.getAndIncrement();
    }
}
