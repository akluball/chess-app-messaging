package com.gitlab.akluball.chessapp.messaging.itest.hook;

import java.io.IOException;

import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.*;

public class DeployUtil {
    public static final String PAYARA_IMAGE = "payara/server-web:5.194";
    public static final String POSTGRES_IMAGE = "postgres:12";
    public static final String MOCKSERVER_IMAGE = "mockserver/mockserver:mockserver-5.8.0";

    public static final String NETWORK_NAME = "chessapp-messaging-net";
    public static final String PAYARA_CONTAINER = "chessapp-messaging-payara";
    public static final String DB_CONTAINER = "chessapp-messaging-db";
    public static final String GAMEHISTORY_CONTAINER = "mock-gamehistory";
    public static final String DB_NAME = "messaging";
    public static final String DB_USER = "postgres";
    public static final String DB_PASSWORD = "postgres";
    public static final String CONTAINER_MESSAGING_WAR = "/chess-app-messaging.war";
    public static final String MESSAGING_DEPLOY_NAME = "chess-app-messaging";

    private static final String[] CONTAINER_EXEC_PREFIX = { "docker", "exec", PAYARA_CONTAINER };
    private static final String[] ASADMIN_PREFIX = { "asadmin", "--user", "admin",
            "--passwordfile", "/opt/payara/passwordFile", "--interactive", "false" };

    public static boolean containerAsadmin(String... asadminCommand) {
        try {
            return 0 == new ProcessBuilder(concat(String.class, CONTAINER_EXEC_PREFIX, ASADMIN_PREFIX, asadminCommand))
                    .inheritIO()
                    .start()
                    .waitFor();
        } catch (InterruptedException | IOException e) {
            throw asRuntime(e);
        }
    }

    public static void copyToContainer(String hostPath, String containerName, String containerPath) {
        String target = String.format("%s:%s", containerName, containerPath);
        try {
            new ProcessBuilder("docker", "cp", hostPath, target)
                    .inheritIO()
                    .start()
                    .waitFor();
        } catch (InterruptedException | IOException e) {
            throw asRuntime(e);
        }
    }

    public static String getContainerIp(String containerName) {
        try {
            Process process = new ProcessBuilder("docker", "inspect", containerName, "--format",
                    String.format("{{json (index .NetworkSettings.Networks \"%s\").IPAddress}}", NETWORK_NAME))
                    .redirectError(ProcessBuilder.Redirect.INHERIT)
                    .start();
            process.waitFor();
            return inputStreamToString(process.getInputStream()).replaceAll("\"", "");
        } catch (IOException | InterruptedException e) {
            throw asRuntime(e);
        }
    }

    public static String getPayaraIp() {
        return getContainerIp(PAYARA_CONTAINER);
    }

    public static String getDbIp() {
        return getContainerIp(DB_CONTAINER);
    }

    public static String getGameHistoryIp() {
        return getContainerIp(GAMEHISTORY_CONTAINER);
    }

    public static String getMessagingUri() {
        return String.format("http://%s:8080/%s", getPayaraIp(), MESSAGING_DEPLOY_NAME);
    }

    public static String getDbUri() {
        return String.format("jdbc:postgresql://%s:5432/%s", getDbIp(), DB_NAME);
    }

    public static String getGameHistoryUri() {
        return String.format("http://%s:1080", getGameHistoryIp());
    }
}
