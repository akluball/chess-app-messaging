package com.gitlab.akluball.chessapp.messaging.itest.security;

import com.gitlab.akluball.chessapp.messaging.itest.TestConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;

public class MessagingSecurity {
    private final RSASSAVerifier verifier;

    @Inject
    MessagingSecurity(TestConfig testConfig) {
        this.verifier = new RSASSAVerifier(testConfig.messagingPublicKey());
    }

    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            return false;
        }
    }
}
