package com.gitlab.akluball.chessapp.messaging.itest.transfer;

public class MessageDto {
    private int userId;
    private String text;
    private long epochSeconds;

    public int getUserId() {
        return userId;
    }

    public String getText() {
        return text;
    }

    public long getEpochSeconds() {
        return epochSeconds;
    }
}
