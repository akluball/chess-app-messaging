package com.gitlab.akluball.chessapp.messaging.itest.param;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.lang.reflect.Executable;
import java.lang.reflect.Parameter;

public class GuicyParameterResolver implements ParameterResolver {
    public static final Injector INJECTOR = Guice.createInjector(new ParameterResolverModule());

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws
            ParameterResolutionException {
        Parameter parameter = parameterContext.getParameter();
        if (parameterContext.isAnnotated(Guicy.class)) {
            return true;
        }
        Executable declaringExecutable = parameter.getDeclaringExecutable();
        if (declaringExecutable.isAnnotationPresent(Guicy.class)) {
            return true;
        }
        Class<?> declaringClass = declaringExecutable.getDeclaringClass();
        return declaringClass.isAnnotationPresent(Guicy.class);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws
            ParameterResolutionException {
        return INJECTOR.getInstance(parameterContext.getParameter().getType());
    }
}
