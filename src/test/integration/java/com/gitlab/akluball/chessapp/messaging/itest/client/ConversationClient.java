package com.gitlab.akluball.chessapp.messaging.itest.client;

import com.gitlab.akluball.chessapp.messaging.itest.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.ConversationInitializerDto;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;
import java.util.Set;

import static com.gitlab.akluball.chessapp.messaging.itest.security.SecurityUtil.bearer;

public class ConversationClient {
    private final WebTarget target;

    @Inject
    ConversationClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target("conversation");
    }

    public Response create(String token, ConversationInitializerDto convInitDto) {
        Invocation.Builder invocationBuilder = this.target.request();
        bearer(invocationBuilder, token);
        return invocationBuilder.post(Entity.json(Objects.nonNull(convInitDto) ? convInitDto : "null"));
    }

    public Response findByConversationId(String token, String conversationId) {
        Invocation.Builder invocationBuilder = this.target.path("" + conversationId).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response findByConversationId(String token, Conversation conversation) {
        return this.findByConversationId(token, "" + conversation.getId());
    }

    public Response findByCurrentUser(String token) {
        Invocation.Builder invocationBuilder = this.target.request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response findBetween(String token, Set<Converser> conversers) {
        Object[] userIds = conversers.stream().map(Converser::getUserId).toArray();
        Invocation.Builder invocationBuilder = this.target.path("between").queryParam("userId", userIds).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }
}
