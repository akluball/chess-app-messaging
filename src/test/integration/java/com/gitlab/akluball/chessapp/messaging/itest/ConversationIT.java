package com.gitlab.akluball.chessapp.messaging.itest;

import com.gitlab.akluball.chessapp.messaging.itest.client.ConversationClient;
import com.gitlab.akluball.chessapp.messaging.itest.data.ConversationDao;
import com.gitlab.akluball.chessapp.messaging.itest.data.ConverserDao;
import com.gitlab.akluball.chessapp.messaging.itest.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;
import com.gitlab.akluball.chessapp.messaging.itest.model.Message;
import com.gitlab.akluball.chessapp.messaging.itest.param.Guicy;
import com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.messaging.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.ConversationDto;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.ConversationInitializerDto;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.messaging.itest.util.Builders;
import com.gitlab.akluball.chessapp.messaging.itest.websocket.ConversationWsClient;
import com.gitlab.akluball.chessapp.messaging.itest.websocket.ConversationWsManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;
import java.time.Instant;

import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.bufferBelow4;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class ConversationIT {
    private final Builders builders;
    private final UniqueBuilders uniqueBuilders;
    private final ConverserDao converserDao;
    private final ConversationDao conversationDao;
    private final UserSecurity userSecurity;
    private final ConversationClient conversationClient;
    private final ConversationWsManager conversationWsManager;

    @Guicy
    ConversationIT(Builders builders,
            UniqueBuilders uniqueBuilders,
            ConverserDao converserDao,
            ConversationDao conversationDao,
            UserSecurity userSecurity,
            ConversationClient conversationClient,
            ConversationWsManager convesationWsManager) {
        this.builders = builders;
        this.uniqueBuilders = uniqueBuilders;
        this.converserDao = converserDao;
        this.conversationDao = conversationDao;
        this.userSecurity = userSecurity;
        this.conversationClient = conversationClient;
        this.conversationWsManager = convesationWsManager;
    }

    @AfterEach
    void cleanup() {
        this.conversationWsManager.close();
    }

    private Converser persistedConverser() {
        Converser converser = this.uniqueBuilders.converser().build();
        this.converserDao.persist(converser);
        return converser;
    }

    @Test
    void create() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto conversationInit = this.builders.convInitDto().from(sender).to(recipient)
                .text("hi").build();
        Response response = this.conversationClient.create(this.userSecurity.tokenFor(sender), conversationInit);
        Conversation created = this.conversationDao.findById(response.readEntity(ConversationDto.class).getId());
        assertThat(created).isNotNull();
    }

    @Test
    void createSetsConversers() {
        Converser sender = this.persistedConverser();
        Converser recipient = this.persistedConverser();
        ConversationInitializerDto conversationInit = this.builders.convInitDto().from(sender).to(recipient)
                .text("hi").build();
        Response response = this.conversationClient.create(this.userSecurity.tokenFor(sender), conversationInit);
        Conversation created = this.conversationDao.findById(response.readEntity(ConversationDto.class).getId());
        assertThat(created.getConversers()).containsExactly(sender, recipient);
    }

    @Test
    void createSetsFirstMessage() {
        Converser sender = this.persistedConverser();
        ConversationInitializerDto conversationInit = this.builders.convInitDto().from(sender)
                .to(this.persistedConverser()).text("first message text").build();
        Response response = this.conversationClient.create(this.userSecurity.tokenFor(sender), conversationInit);
        Conversation created = this.conversationDao.findById(response.readEntity(ConversationDto.class).getId());
        assertThat(created.getMessages()).hasSize(1);
        Message firstMessage = created.getMessages().get(0);
        assertThat(firstMessage.getText()).isEqualTo("first message text");
        assertThat(firstMessage.getUserId()).isEqualTo(sender.getUserId());
        assertThat(firstMessage.getEpochSeconds()).isIn(bufferBelow4(Instant.now().getEpochSecond()));
    }

    @Test
    void sendUserId() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(token, conversation);
        wsClient.sendMessage("sent message text");
        wsClient.nextMessage();
        this.conversationDao.refresh(conversation);
        assertThat(conversation.getLastMessage().getUserId()).isEqualTo(converser.getUserId());
    }

    @Test
    void sendMessageText() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(token, conversation);
        wsClient.sendMessage("sent message text");
        wsClient.nextMessage();
        this.conversationDao.refresh(conversation);
        assertThat(conversation.getLastMessage().getText()).isEqualTo("sent message text");
    }

    @Test
    void sendEpochSeconds() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(token, conversation);
        wsClient.sendMessage("sent message text");
        wsClient.nextMessage();
        this.conversationDao.refresh(conversation);
        assertThat(conversation.getLastMessage().getEpochSeconds()).isIn(bufferBelow4(Instant.now().getEpochSecond()));
    }

    @Test
    void sendEmptyMessageNotPersisted() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converser);
        ConversationWsClient wsClient = this.conversationWsManager.createClient(token, conversation);
        wsClient.sendMessage("");
        wsClient.nextMessage();
        this.conversationDao.refresh(conversation);
        assertThat(conversation.getMessages()).hasSize(0);
    }
}
