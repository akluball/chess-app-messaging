package com.gitlab.akluball.chessapp.messaging.itest.client;

import com.gitlab.akluball.chessapp.messaging.itest.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.GameHistoryDto;

import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static com.gitlab.akluball.chessapp.messaging.itest.security.SecurityUtil.bearer;

public class GameConversationClient {
    private final WebTarget target;

    @Inject
    GameConversationClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target("gameconversation");
    }

    public Response findByGameHistoryId(String token, String gameHistoryId) {
        Invocation.Builder invocationBuilder = this.target.path(gameHistoryId).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response findByGameHistoryId(String token, Integer gameHistoryId) {
        return this.findByGameHistoryId(token, "" + gameHistoryId);
    }

    public Response findByGameHistoryId(String token, GameConversation gameConversation) {
        return this.findByGameHistoryId(token, gameConversation.getGameHistoryId());
    }

    public Response findByGameHistoryId(String token, GameHistoryDto gameHistoryDto) {
        return this.findByGameHistoryId(token, gameHistoryDto.getId());
    }
}
