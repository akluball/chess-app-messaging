package com.gitlab.akluball.chessapp.messaging.itest.security;

import com.nimbusds.jwt.SignedJWT;

import javax.ws.rs.client.Invocation;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.text.ParseException;

import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.asRuntime;

public class SecurityUtil {
    public static void bearer(Invocation.Builder invocatioBuilder, String token) {
        if (token != null) {
            invocatioBuilder.header("Authorization", String.format("Bearer %s", token));
        }
    }

    public static PrivateKey rsaPrivateKey() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            return keyPairGenerator.generateKeyPair().getPrivate();
        } catch (NoSuchAlgorithmException e) {
            throw asRuntime(e);
        }
    }

    public static SignedJWT parseSignedToken(String serializedToken) {
        try {
            return SignedJWT.parse(serializedToken);
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }
}
