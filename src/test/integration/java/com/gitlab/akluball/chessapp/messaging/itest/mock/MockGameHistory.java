package com.gitlab.akluball.chessapp.messaging.itest.mock;

import com.gitlab.akluball.chessapp.messaging.itest.TestConfig;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.GameHistoryDto;
import com.nimbusds.jwt.SignedJWT;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.bind.Jsonb;

import static com.gitlab.akluball.chessapp.messaging.itest.mock.MockUtil.*;

@Singleton
public class MockGameHistory {
    private final MockServerClient client;
    private final Jsonb jsonb;

    @Inject
    MockGameHistory(TestConfig testConfig, Jsonb jsonb) {
        this.jsonb = jsonb;
        this.client = new MockServerClient(testConfig.gameHistoryHost(), testConfig.gameHistoryPort());
    }

    private static HttpRequest createGetRequest(int gameHistoryId) {
        return HttpRequest.request()
                .withPath(String.format("/api/gamehistory/%s", gameHistoryId));
    }

    public void reset() {
        this.client.reset();
    }

    public void expectGet(GameHistoryDto gameHistoryDto) {
        HttpRequest req = createGetRequest(gameHistoryDto.getId());
        String resBody = this.jsonb.toJson(gameHistoryDto);
        HttpResponse res = HttpResponse.response().withHeader(APPLICATION_JSON).withBody(resBody);
        this.client.when(req).respond(res);
    }

    public void expectGetNotFound(int gameHistoryId) {
        HttpRequest req = createGetRequest(gameHistoryId);
        HttpResponse res = HttpResponse.response().withStatusCode(404);
        this.client.when(req).respond(res);
    }

    public SignedJWT retrieveGetToken(int gameHistoryId) {
        return extractToken(this.client, createGetRequest(gameHistoryId));
    }

    public void verifyGet(int gameHistoryId) {
        HttpRequest req = createGetRequest(gameHistoryId);
        verifyWithRetry(this.client, req);
        this.client.clear(req);
    }

    public void verifyGet(GameHistoryDto gameHistoryDto) {
        this.verifyGet(gameHistoryDto.getId());
    }

    public void clearGet(int gameHistoryId) {
        this.client.clear(createGetRequest(gameHistoryId));
    }

    public void clearGet(GameHistoryDto gameHistoryDto) {
        this.clearGet(gameHistoryDto.getId());
    }
}
