package com.gitlab.akluball.chessapp.messaging.itest;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Properties;

import static com.gitlab.akluball.chessapp.messaging.itest.hook.DeployUtil.*;

@Singleton
public class TestConfig {
    public static final String MESSAGING_PUBLICKEY_PROP = "chessapp.messaging.publickey";
    public static final String USER_PRIVATEKEY_PROP = "chessapp.user.privatekey";
    private final String messagingUri;
    private final String dbUri;
    private final String dbUser;
    private final String dbPassword;
    private final String gameHistoryHost;
    private final RSAPublicKey messagingPublicKey;
    private final PrivateKey userPrivateKey;

    public TestConfig() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        this.messagingUri = getMessagingUri();
        this.dbUri = getDbUri();
        this.dbUser = DB_USER;
        this.dbPassword = DB_PASSWORD;
        this.gameHistoryHost = getGameHistoryIp();
        try (InputStream configStream = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("messaging-test.properties")) {
            Properties config = new Properties();
            config.load(configStream);
            Base64.Decoder decoder = Base64.getDecoder();
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] messagingPublicKeyBytes = decoder.decode(config.getProperty(MESSAGING_PUBLICKEY_PROP));
            this.messagingPublicKey = (RSAPublicKey) keyFactory
                    .generatePublic(new X509EncodedKeySpec(messagingPublicKeyBytes));
            byte[] userPrivateKeyBytes = decoder.decode(config.getProperty(USER_PRIVATEKEY_PROP));
            this.userPrivateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(userPrivateKeyBytes));
        }
    }

    public String messagingUri() {
        return this.messagingUri;
    }

    public String dbUri() {
        return this.dbUri;
    }

    public String dbUser() {
        return this.dbUser;
    }

    public String dbPassword() {
        return this.dbPassword;
    }

    public String gameHistoryHost() {
        return this.gameHistoryHost;
    }

    public int gameHistoryPort() {
        return 1080;
    }

    public RSAPublicKey messagingPublicKey() {
        return this.messagingPublicKey;
    }

    public PrivateKey userPrivateKey() {
        return this.userPrivateKey;
    }
}
