package com.gitlab.akluball.chessapp.messaging.itest.unique;

import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;
import com.gitlab.akluball.chessapp.messaging.itest.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.GameHistoryDto;
import com.gitlab.akluball.chessapp.messaging.itest.util.Builders;

import javax.inject.Inject;

public class UniqueBuilders {
    private final UniqueData uniqueData;
    private final Builders builders;

    @Inject
    UniqueBuilders(UniqueData uniqueData, Builders builders) {
        this.uniqueData = uniqueData;
        this.builders = builders;
    }

    public Converser.Builder converser() {
        return this.builders.converser()
                .userId(this.uniqueData.userId());
    }

    public GameConversation.Builder gameConversation() {
        return this.builders.gameConversation()
                .gameHistoryId(this.uniqueData.gameHistoryId())
                .whiteSideUserId(this.uniqueData.userId())
                .blackSideUserId(this.uniqueData.userId());
    }

    public GameHistoryDto.Builder gameHistoryDto() {
        return this.builders.gameHistoryDto()
                .id(this.uniqueData.gameHistoryId())
                .whiteSideUserId(this.uniqueData.userId())
                .blackSideUserId(this.uniqueData.userId());
    }
}
