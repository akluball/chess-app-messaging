package com.gitlab.akluball.chessapp.messaging.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;

@Embeddable
@Access(AccessType.FIELD)
public class Message {
    private int userId;
    private String text;
    private long epochSeconds;

    public int getUserId() {
        return userId;
    }

    public String getText() {
        return this.text;
    }

    public long getEpochSeconds() {
        return epochSeconds;
    }

    public static class Builder {
        private final Message message;

        Builder() {
            this.message = new Message();
        }

        public Builder from(int userId) {
            this.message.userId = userId;
            return this;
        }

        public Builder from(Converser converser) {
            this.message.userId = converser.getUserId();
            return this;
        }

        public Builder text(String text) {
            this.message.text = text;
            return this;
        }

        public Builder epochSeconds(long epochSeconds) {
            this.message.epochSeconds = epochSeconds;
            return this;
        }

        public Message build() {
            return this.message;
        }
    }
}
