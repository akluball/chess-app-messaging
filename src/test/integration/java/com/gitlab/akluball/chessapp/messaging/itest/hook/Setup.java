package com.gitlab.akluball.chessapp.messaging.itest.hook;

import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;

import static com.gitlab.akluball.chessapp.messaging.itest.TestConfig.MESSAGING_PUBLICKEY_PROP;
import static com.gitlab.akluball.chessapp.messaging.itest.TestConfig.USER_PRIVATEKEY_PROP;
import static com.gitlab.akluball.chessapp.messaging.itest.hook.DeployUtil.*;
import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.sleepMillis;

public class Setup {
    public static void main(String[] args) throws IOException, InterruptedException, NoSuchAlgorithmException {
        // keys
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        Base64.Encoder encoder = Base64.getEncoder();
        // messaging keys
        KeyPair messagingKeyPair = keyPairGenerator.generateKeyPair();
        byte[] messagingPrivateKeyBytes = encoder.encode(messagingKeyPair.getPrivate().getEncoded());
        String messagingPrivateKey = new String(messagingPrivateKeyBytes);
        byte[] messagingPublicKeyBytes = encoder.encode(messagingKeyPair.getPublic().getEncoded());
        String messagingPublicKey = new String(messagingPublicKeyBytes);
        // user keys
        KeyPair userKeyPair = keyPairGenerator.generateKeyPair();
        byte[] userPrivateKeyBytes = encoder.encode(userKeyPair.getPrivate().getEncoded());
        String userPrivateKey = new String(userPrivateKeyBytes);
        byte[] userPublicKeyBytes = encoder.encode(userKeyPair.getPublic().getEncoded());
        String userPublicKey = new String(userPublicKeyBytes);
        // store keys for test runs
        Properties testProps = new Properties();
        testProps.put(MESSAGING_PUBLICKEY_PROP, messagingPublicKey);
        testProps.put(USER_PRIVATEKEY_PROP, userPrivateKey);
        FileWriter testPropsWriter = new FileWriter(System.getenv("MESSAGING_TEST_PROPERTIES"));
        testProps.store(testPropsWriter, null);
        // containers
        new ProcessBuilder("docker", "network", "create", NETWORK_NAME)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", DB_CONTAINER, "--network", NETWORK_NAME,
                "--env", String.format("POSTGRES_DB=%s", DB_NAME), "--env", String.format("POSTGRES_USER=%s", DB_USER),
                "--env", String.format("POSTGRES_PASSWORD=%s", DB_PASSWORD), "--rm", "--detach",
                POSTGRES_IMAGE,
                "-c", "max_connections=200")
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", GAMEHISTORY_CONTAINER, "--network", NETWORK_NAME,
                "--detach", "--rm", MOCKSERVER_IMAGE)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", PAYARA_CONTAINER, "--network", NETWORK_NAME,
                "--env", String.format("DB_SERVER=%s", getDbIp()),
                "--env", String.format("DB_NAME=%s", DB_NAME),
                "--env", String.format("DB_USER=%s", DB_USER),
                "--env", String.format("DB_PASSWORD=%s", DB_PASSWORD),
                "--env", String.format("CHESSAPP_GAMEHISTORY_URI=%s", getGameHistoryUri()),
                "--env", String.format("CHESSAPP_MESSAGING_PRIVATEKEY=%s", messagingPrivateKey),
                "--env", String.format("CHESSAPP_USER_PUBLICKEY=%s", userPublicKey),
                "--rm", "--detach", PAYARA_IMAGE)
                .inheritIO().start().waitFor();
        String messagingWar = System.getenv("CHESSAPP_MESSAGING_WAR");
        copyToContainer(messagingWar, PAYARA_CONTAINER, CONTAINER_MESSAGING_WAR);
        // wait for payara to come up
        int uptimeAttempts = 6;
        while (uptimeAttempts > 0 && !containerAsadmin("uptime")) {
            sleepMillis(500);
            uptimeAttempts--;
        }
        containerAsadmin("deploy", "--name", MESSAGING_DEPLOY_NAME, CONTAINER_MESSAGING_WAR);
    }
}
