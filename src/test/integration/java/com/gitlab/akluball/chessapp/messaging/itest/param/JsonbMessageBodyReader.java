package com.gitlab.akluball.chessapp.messaging.itest.param;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Provider
public class JsonbMessageBodyReader implements MessageBodyReader<Object> {
    private final Jsonb jsonb;

    @Inject
    JsonbMessageBodyReader(Jsonb jsonb) {
        this.jsonb = jsonb;
    }

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return mediaType.equals(MediaType.valueOf(MediaType.APPLICATION_JSON));
    }

    @Override
    public Object readFrom(Class<Object> type,
            Type genericType,
            Annotation[] annotations,
            MediaType mediaType,
            MultivaluedMap<String, String> httpHeaders,
            InputStream entityStream) throws IOException, WebApplicationException {
        return this.jsonb.fromJson(entityStream, genericType);
    }
}
