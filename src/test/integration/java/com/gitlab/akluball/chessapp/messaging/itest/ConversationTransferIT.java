package com.gitlab.akluball.chessapp.messaging.itest;

import com.gitlab.akluball.chessapp.messaging.itest.client.ConversationClient;
import com.gitlab.akluball.chessapp.messaging.itest.data.ConversationDao;
import com.gitlab.akluball.chessapp.messaging.itest.data.ConverserDao;
import com.gitlab.akluball.chessapp.messaging.itest.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;
import com.gitlab.akluball.chessapp.messaging.itest.param.Guicy;
import com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.messaging.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.ConversationDto;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.MessageDto;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.messaging.itest.util.Builders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class ConversationTransferIT {
    private final Builders builders;
    private final UniqueBuilders uniqueBuilders;
    private final ConverserDao converserDao;
    private final ConversationDao conversationDao;
    private final UserSecurity userSecurity;
    private final ConversationClient conversationClient;

    @Guicy
    ConversationTransferIT(Builders builders,
            UniqueBuilders uniqueBuilders,
            ConverserDao converserDao,
            ConversationDao conversationDao,
            UserSecurity userSecurity,
            ConversationClient conversationClient) {
        this.builders = builders;
        this.uniqueBuilders = uniqueBuilders;
        this.converserDao = converserDao;
        this.conversationDao = conversationDao;
        this.userSecurity = userSecurity;
        this.conversationClient = conversationClient;
    }

    private Converser persistedConverser() {
        Converser converser = this.uniqueBuilders.converser().build();
        this.converserDao.persist(converser);
        return converser;
    }

    @Test
    void id() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converser);
        Response response = this.conversationClient.findByConversationId(token, conversation);
        assertThat(response.readEntity(ConversationDto.class).getId()).isEqualTo(conversation.getId());
    }

    @Test
    void converserIds() {
        Converser converserOne = this.persistedConverser();
        Converser converserTwo = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converserOne).converser(converserTwo)
                .build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converserOne);
        Response response = this.conversationClient.findByConversationId(token, conversation);
        assertThat(response.readEntity(ConversationDto.class).getConverserIds())
                .containsExactly(converserOne.getUserId(), converserTwo.getUserId());
    }

    @Test
    void messageUserId() {
        Converser converserOne = this.persistedConverser();
        Converser converserTwo = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converserOne).converser(converserTwo)
                .message(this.builders.message().text("first message text").from(converserOne).build())
                .message(this.builders.message().text("second message text").from(converserTwo).build()).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converserOne);
        Response response = this.conversationClient.findByConversationId(token, conversation);
        List<MessageDto> messages = response.readEntity(ConversationDto.class).getMessages();
        assertThat(messages).hasSize(2);
        assertThat(messages.stream().map(MessageDto::getUserId).collect(Collectors.toList()))
                .containsExactly(converserOne.getUserId(), converserTwo.getUserId()).inOrder();
    }

    @Test
    void messageText() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser)
                .message(this.builders.message().text("first message text").build())
                .message(this.builders.message().text("second message text").build()).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converser);
        Response response = this.conversationClient.findByConversationId(token, conversation);
        List<MessageDto> messages = response.readEntity(ConversationDto.class).getMessages();
        assertThat(messages).hasSize(2);
        assertThat(messages.stream().map(MessageDto::getText).collect(Collectors.toList()))
                .containsExactly("first message text", "second message text").inOrder();
    }

    @Test
    void epochSeconds() {
        Converser converser = this.persistedConverser();
        Conversation conversation = this.builders.conversation().converser(converser)
                .message(this.builders.message().epochSeconds(5000L).build())
                .message(this.builders.message().epochSeconds(6000L).build()).build();
        this.conversationDao.persist(conversation);
        String token = this.userSecurity.tokenFor(converser);
        Response response = this.conversationClient.findByConversationId(token, conversation);
        List<MessageDto> messages = response.readEntity(ConversationDto.class).getMessages();
        assertThat(messages).hasSize(2);
        assertThat(messages.stream().map(MessageDto::getEpochSeconds).collect(Collectors.toList()))
                .containsExactly(5000L, 6000L).inOrder();
    }
}
