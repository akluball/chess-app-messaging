package com.gitlab.akluball.chessapp.messaging.itest.hook;

import com.gitlab.akluball.chessapp.messaging.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.messaging.itest.param.ParameterResolverModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import static com.gitlab.akluball.chessapp.messaging.itest.hook.DeployUtil.*;

public class Before {
    public static void main(String[] args) {
        String messagingWar = System.getenv("CHESSAPP_MESSAGING_WAR");
        copyToContainer(messagingWar, PAYARA_CONTAINER, CONTAINER_MESSAGING_WAR);
        containerAsadmin("redeploy", "--name", MESSAGING_DEPLOY_NAME, CONTAINER_MESSAGING_WAR);
        Injector injector = Guice.createInjector(new ParameterResolverModule());
        injector.getInstance(MockGameHistory.class).reset();
        EntityManager entityManager = injector.getInstance(EntityManager.class);
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.flush();
        tx.commit();
    }
}
