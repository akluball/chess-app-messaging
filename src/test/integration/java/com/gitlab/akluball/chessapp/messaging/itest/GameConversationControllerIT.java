package com.gitlab.akluball.chessapp.messaging.itest;

import com.gitlab.akluball.chessapp.messaging.itest.client.GameConversationClient;
import com.gitlab.akluball.chessapp.messaging.itest.data.GameConversationDao;
import com.gitlab.akluball.chessapp.messaging.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.messaging.itest.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.itest.param.Guicy;
import com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.messaging.itest.security.MessagingSecurity;
import com.gitlab.akluball.chessapp.messaging.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.GameConversationDto;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.GameHistoryDto;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueData;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameConversationControllerIT {
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final GameConversationDao gameConversationDao;
    private final MockGameHistory mockGameHistory;
    private final UserSecurity userSecurity;
    private final GameConversationClient gameConversationClient;

    @Guicy
    GameConversationControllerIT(UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            GameConversationDao gameConversationDao,
            MockGameHistory mockGameHistory,
            UserSecurity userSecurity,
            GameConversationClient gameConversationClient) {
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.gameConversationDao = gameConversationDao;
        this.mockGameHistory = mockGameHistory;
        this.userSecurity = userSecurity;
        this.gameConversationClient = gameConversationClient;
    }

    private GameConversation persistedGameConversation() {
        GameConversation gameConversation = this.uniqueBuilders.gameConversation().build();
        this.gameConversationDao.persist(gameConversation);
        return gameConversation;
    }

    @Test
    void findByIdAlreadyExists() {
        GameConversation gameConversation = this.persistedGameConversation();
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameConversation);
        assertThat(response.getStatus()).isEqualTo(200);
        GameConversationDto found = response.readEntity(GameConversationDto.class);
        assertThat(found.getGameHistoryId()).isEqualTo(gameConversation.getGameHistoryId());
    }

    @Test
    void findByIdNotCreatedYet() {
        GameHistoryDto gameHistoryDto = this.uniqueBuilders.gameHistoryDto().build();
        this.mockGameHistory.expectGet(gameHistoryDto);
        String token = this.userSecurity.tokenFor(gameHistoryDto.getBlackSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameHistoryDto);
        assertThat(response.getStatus()).isEqualTo(200);
        GameConversationDto created = response.readEntity(GameConversationDto.class);
        assertThat(created.getGameHistoryId()).isEqualTo(gameHistoryDto.getId());
        this.mockGameHistory.verifyGet(gameHistoryDto);
    }

    @Test
    @Guicy
    void findByIdNotCreatedMessagingAuth(MessagingSecurity messagingSecurity) {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        int gameHistoryId = this.uniqueData.gameHistoryId();
        this.gameConversationClient.findByGameHistoryId(token, gameHistoryId);
        SignedJWT messagingToken = this.mockGameHistory.retrieveGetToken(gameHistoryId);
        assertThat(messagingSecurity.isValidSignature(messagingToken)).isTrue();
        this.mockGameHistory.clearGet(gameHistoryId);
    }

    @Test
    void findByIdNoAuth() {
        GameConversation gameConversation = this.persistedGameConversation();
        Response response = this.gameConversationClient.findByGameHistoryId(null, gameConversation);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByIdBadRole() {
        GameConversation gameConversation = this.persistedGameConversation();
        String badRoleToken = this.userSecurity.badRoleTokenFor(gameConversation.getBlackSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(badRoleToken, gameConversation);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByIdNotInteger() {
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void findByIdBadSignature() {
        GameConversation gameConversation = this.persistedGameConversation();
        String badSignatureToken = this.userSecurity.badSignatureTokenFor(gameConversation.getBlackSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(badSignatureToken, gameConversation);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByIdNotParticipant() {
        GameConversation gameConversation = this.persistedGameConversation();
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameConversation);
        assertThat(response.getStatus()).isEqualTo(403);
        assertThat(response.readEntity(String.class)).containsMatch("not authorized");
    }

    @Test
    void findByIdDoesNotExist() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        this.mockGameHistory.expectGetNotFound(gameHistoryId);
        String token = this.userSecurity.tokenFor(this.uniqueData.userId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameHistoryId);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
        this.mockGameHistory.verifyGet(gameHistoryId);
    }
}
