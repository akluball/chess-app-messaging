package com.gitlab.akluball.chessapp.messaging.itest.data;

import com.gitlab.akluball.chessapp.messaging.itest.model.Converser;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.stream.Stream;

public class ConverserDao {
    private final EntityManager entityManager;

    @Inject
    ConverserDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(Converser... conversers) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        Stream.of(conversers).forEach(this.entityManager::persist);
        tx.commit();
    }

    public Converser merge(Converser converser) {
        EntityTransaction tx = this.entityManager.getTransaction();
        boolean isActive = tx.isActive();
        if (!isActive) {
            tx.begin();
        }
        Converser merged = this.entityManager.merge(converser);
        if (!isActive) {
            tx.commit();
        }
        return merged;
    }

    public void refresh(Converser converser) {
        this.entityManager.refresh(converser);
    }
}
