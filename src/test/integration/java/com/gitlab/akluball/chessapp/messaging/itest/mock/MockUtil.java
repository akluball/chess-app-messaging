package com.gitlab.akluball.chessapp.messaging.itest.mock;

import com.nimbusds.jwt.SignedJWT;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;

import javax.json.bind.Jsonb;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver.INJECTOR;
import static com.gitlab.akluball.chessapp.messaging.itest.security.SecurityUtil.parseSignedToken;
import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.sleepMillis;
import static com.google.common.truth.Truth.assertThat;

public class MockUtil {
    static final Header APPLICATION_JSON = Header.header("Content-Type", "application/json");
    private static final int RETRY_ATTEMPTS = 3;
    private static final int WAIT_MILLIS = 100;

    static SignedJWT extractToken(MockServerClient client, HttpRequest req) {
        HttpRequest[] recordedRequests = {};
        int attemptsRemaining = RETRY_ATTEMPTS;
        while (attemptsRemaining > 0) {
            recordedRequests = client.retrieveRecordedRequests(req);
            if (recordedRequests.length > 0 || attemptsRemaining == 1) {
                break;
            }
            sleepMillis(WAIT_MILLIS);
            attemptsRemaining--;
        }
        assertThat(recordedRequests).asList().hasSize(1);
        List<String> authHeaders = recordedRequests[0].getHeader("Authorization");
        assertThat(authHeaders).hasSize(1);
        String authHeader = authHeaders.get(0);
        assertThat(authHeader).startsWith("Bearer ");
        return parseSignedToken(authHeader.replace("Bearer ", ""));
    }

    static <T> T extractRequestBody(MockServerClient client, HttpRequest req, Class<T> bodyClass) {
        HttpRequest[] recordedRequests = {};
        int attemptsRemaining = RETRY_ATTEMPTS;
        while (attemptsRemaining > 0) {
            recordedRequests = client.retrieveRecordedRequests(req);
            if (recordedRequests.length > 0 || attemptsRemaining == 1) {
                break;
            }
            sleepMillis(WAIT_MILLIS);
            attemptsRemaining--;
        }
        assertThat(recordedRequests).asList().hasSize(1);
        return INJECTOR.getInstance(Jsonb.class)
                .fromJson(recordedRequests[0].getBodyAsString(), bodyClass);
    }

    static <T> List<T> extractRequestBodies(MockServerClient client, HttpRequest req, int count, Class<T> bodyClass) {
        HttpRequest[] recordedRequests = {};
        int attemptsRemaining = RETRY_ATTEMPTS;
        while (attemptsRemaining > 0) {
            recordedRequests = client.retrieveRecordedRequests(req);
            if (recordedRequests.length >= count || attemptsRemaining == 1) {
                break;
            }
            sleepMillis(WAIT_MILLIS);
            attemptsRemaining--;
        }
        assertThat(recordedRequests).asList().hasSize(count);
        Jsonb jsonb = INJECTOR.getInstance(Jsonb.class);
        return Stream.of(recordedRequests)
                .map(HttpRequest::getBodyAsString)
                .map(asString -> jsonb.fromJson(asString, bodyClass))
                .collect(Collectors.toList());
    }

    static void verifyWithRetry(MockServerClient client, HttpRequest req) {
        int attemptsRemaining = RETRY_ATTEMPTS;
        while (attemptsRemaining > 0) {
            try {
                client.verify(req);
                break;
            } catch (AssertionError e) {
                if (attemptsRemaining == 1) {
                    throw e;
                }
            }
            attemptsRemaining--;
        }
    }
}
