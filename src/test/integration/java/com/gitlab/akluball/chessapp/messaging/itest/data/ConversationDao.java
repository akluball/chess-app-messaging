package com.gitlab.akluball.chessapp.messaging.itest.data;

import com.gitlab.akluball.chessapp.messaging.itest.model.Conversation;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Set;
import java.util.stream.Stream;

public class ConversationDao {
    private final EntityManager entityManager;
    private final ConverserDao converserDao;

    @Inject
    ConversationDao(EntityManager entityManager, ConverserDao converserDao) {
        this.entityManager = entityManager;
        this.converserDao = converserDao;
    }

    public Conversation findById(int id) {
        return this.entityManager.find(Conversation.class, id);
    }

    public void persist(Conversation... conversations) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        Stream.of(conversations).forEach(this.entityManager::persist);
        Stream.of(conversations).map(Conversation::getConversers)
                .flatMap(Set::stream)
                .distinct()
                .forEach(this.converserDao::merge);
        tx.commit();
        Stream.of(conversations).map(Conversation::getConversers)
                .flatMap(Set::stream)
                .distinct()
                .forEach(this.converserDao::refresh);
    }

    public void refresh(Conversation conversation) {
        this.entityManager.refresh(conversation);
    }

    public void delete(Conversation conversation) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.remove(conversation);
        conversation.getConversers().forEach(this.entityManager::refresh);
        conversation.getConversers().forEach(converser -> converser.exit(conversation));
        tx.commit();
    }
}
