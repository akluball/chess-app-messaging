package com.gitlab.akluball.chessapp.messaging.itest;

import com.gitlab.akluball.chessapp.messaging.itest.client.GameConversationClient;
import com.gitlab.akluball.chessapp.messaging.itest.data.GameConversationDao;
import com.gitlab.akluball.chessapp.messaging.itest.mock.MockGameHistory;
import com.gitlab.akluball.chessapp.messaging.itest.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.itest.param.Guicy;
import com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.messaging.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.GameHistoryDto;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.messaging.itest.websocket.GameConversationWsClient;
import com.gitlab.akluball.chessapp.messaging.itest.websocket.GameConversationWsManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.Instant;

import static com.gitlab.akluball.chessapp.messaging.itest.util.Util.bufferBelow4;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameConversationIT {
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final GameConversationDao gameConversationDao;
    private final MockGameHistory mockGameHistory;
    private final UserSecurity userSecurity;
    private final GameConversationClient gameConversationClient;
    private final GameConversationWsManager gameConversationWsManager;

    @Guicy
    GameConversationIT(UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            GameConversationDao gameConversationDao,
            MockGameHistory mockGameHistory,
            UserSecurity userSecurity,
            GameConversationClient gameConversationClient,
            GameConversationWsManager gameConversationWsManager) {
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.gameConversationDao = gameConversationDao;
        this.mockGameHistory = mockGameHistory;
        this.userSecurity = userSecurity;
        this.gameConversationClient = gameConversationClient;
        this.gameConversationWsManager = gameConversationWsManager;
    }

    @AfterEach
    void cleanup() {
        this.gameConversationWsManager.close();
    }

    private GameConversation persistedGameConversation() {
        GameConversation gameConversation = this.uniqueBuilders.gameConversation().build();
        this.gameConversationDao.persist(gameConversation);
        return gameConversation;
    }

    @Test
    void createPersists() {
        GameHistoryDto gameHistoryDto = this.uniqueBuilders.gameHistoryDto().build();
        this.mockGameHistory.expectGet(gameHistoryDto);
        String token = this.userSecurity.tokenFor(gameHistoryDto.getBlackSideUserId());
        this.gameConversationClient.findByGameHistoryId(token, gameHistoryDto);
        assertThat(gameConversationDao.findByGameHistoryId(gameHistoryDto)).isNotNull();
        this.mockGameHistory.clearGet(gameHistoryDto);
    }

    @Test
    void createSetsWhiteSideUserId() {
        int whiteSideUserId = this.uniqueData.userId();
        GameHistoryDto gameHistoryDto = this.uniqueBuilders.gameHistoryDto().whiteSideUserId(whiteSideUserId).build();
        this.mockGameHistory.expectGet(gameHistoryDto);
        String token = this.userSecurity.tokenFor(gameHistoryDto.getBlackSideUserId());
        this.gameConversationClient.findByGameHistoryId(token, gameHistoryDto);
        assertThat(gameConversationDao.findByGameHistoryId(gameHistoryDto).getWhiteSideUserId())
                .isEqualTo(whiteSideUserId);
        this.mockGameHistory.clearGet(gameHistoryDto);
    }

    @Test
    void createSetsBlackSideUserId() {
        int blackSideUserId = this.uniqueData.userId();
        GameHistoryDto gameHistoryDto = this.uniqueBuilders.gameHistoryDto().blackSideUserId(blackSideUserId).build();
        this.mockGameHistory.expectGet(gameHistoryDto);
        String token = this.userSecurity.tokenFor(gameHistoryDto.getBlackSideUserId());
        this.gameConversationClient.findByGameHistoryId(token, gameHistoryDto);
        assertThat(gameConversationDao.findByGameHistoryId(gameHistoryDto).getBlackSideUserId())
                .isEqualTo(blackSideUserId);
        this.mockGameHistory.clearGet(gameHistoryDto);
    }

    @Test
    void sendText() {
        GameConversation gameConversation = this.persistedGameConversation();
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(token, gameConversation);
        wsClient.sendMessage("sent message text");
        wsClient.nextMessage();
        this.gameConversationDao.refresh(gameConversation);
        assertThat(gameConversation.getLastMessage().getText()).isEqualTo("sent message text");
    }

    @Test
    void sendWhiteSideUser() {
        GameConversation gameConversation = this.persistedGameConversation();
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(token, gameConversation);
        wsClient.sendMessage("arbitrary text");
        wsClient.nextMessage();
        this.gameConversationDao.refresh(gameConversation);
        assertThat(gameConversation.getLastMessage().getUserId()).isEqualTo(gameConversation.getWhiteSideUserId());
    }

    @Test
    void sendBlackSideUser() {
        GameConversation gameConversation = this.persistedGameConversation();
        String token = this.userSecurity.tokenFor(gameConversation.getBlackSideUserId());
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(token, gameConversation);
        wsClient.sendMessage("arbitrary text");
        wsClient.nextMessage();
        this.gameConversationDao.refresh(gameConversation);
        assertThat(gameConversation.getLastMessage().getUserId()).isEqualTo(gameConversation.getBlackSideUserId());
    }

    @Test
    void sendEpochSeconds() {
        GameConversation gameConversation = this.persistedGameConversation();
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(token, gameConversation);
        wsClient.sendMessage("arbitrary text");
        wsClient.nextMessage();
        this.gameConversationDao.refresh(gameConversation);
        assertThat(gameConversation.getLastMessage().getEpochSeconds())
                .isIn(bufferBelow4(Instant.now().getEpochSecond()));
    }

    @Test
    void sendEmptyMessageNotPersisted() {
        GameConversation gameConversation = this.persistedGameConversation();
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        GameConversationWsClient wsClient = this.gameConversationWsManager.createClient(token, gameConversation);
        wsClient.sendMessage("");
        wsClient.nextMessage();
        this.gameConversationDao.refresh(gameConversation);
        assertThat(gameConversation.getMessages()).hasSize(0);
    }
}
