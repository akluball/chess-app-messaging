package com.gitlab.akluball.chessapp.messaging.itest.param;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Provider
public class JsonbMessageBodyWriter implements MessageBodyWriter<Object> {
    private final Jsonb jsonb;

    @Inject
    JsonbMessageBodyWriter(Jsonb jsonb) {
        this.jsonb = jsonb;
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return mediaType.equals(MediaType.valueOf(MediaType.APPLICATION_JSON));
    }

    @Override
    public void writeTo(Object object,
            Class<?> type,
            Type genericType,
            Annotation[] annotations,
            MediaType mediaType,
            MultivaluedMap<String, Object> httpHeaders,
            OutputStream entityStream) throws IOException, WebApplicationException {
        this.jsonb.toJson(object, entityStream);
    }
}
