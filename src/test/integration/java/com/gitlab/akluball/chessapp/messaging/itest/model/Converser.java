package com.gitlab.akluball.chessapp.messaging.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@Access(AccessType.FIELD)
public class Converser {
    @Id
    private int userId;
    @ManyToMany
    private Set<Conversation> conversations = new HashSet<>();

    public int getUserId() {
        return userId;
    }

    public void participantIn(Conversation conversation) {
        this.conversations.add(conversation);
    }

    public void exit(Conversation conversation) {
        this.conversations.remove(conversation);
    }

    public static class Builder {
        private final Converser converser;

        Builder() {
            this.converser = new Converser();
        }

        public Builder userId(int userId) {
            this.converser.userId = userId;
            return this;
        }

        public Converser build() {
            return this.converser;
        }
    }
}
