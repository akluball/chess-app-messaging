package com.gitlab.akluball.chessapp.messaging.itest;

import com.gitlab.akluball.chessapp.messaging.itest.client.GameConversationClient;
import com.gitlab.akluball.chessapp.messaging.itest.data.GameConversationDao;
import com.gitlab.akluball.chessapp.messaging.itest.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.itest.param.Guicy;
import com.gitlab.akluball.chessapp.messaging.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.messaging.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.GameConversationDto;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.MessageDto;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.messaging.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.messaging.itest.util.Builders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class GameConversationTransferIT {
    private final Builders builders;
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final GameConversationDao gameConversationDao;
    private final UserSecurity userSecurity;
    private final GameConversationClient gameConversationClient;

    @Guicy
    GameConversationTransferIT(Builders builders,
            UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            GameConversationDao gameConversationDao,
            UserSecurity userSecurity,
            GameConversationClient gameConversationClient) {
        this.builders = builders;
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.gameConversationDao = gameConversationDao;
        this.userSecurity = userSecurity;
        this.gameConversationClient = gameConversationClient;
    }

    @Test
    void gameHistoryId() {
        int gameHistoryId = this.uniqueData.gameHistoryId();
        GameConversation gameConversation = this.uniqueBuilders.gameConversation().gameHistoryId(gameHistoryId).build();
        this.gameConversationDao.persist(gameConversation);
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameConversation);
        assertThat(response.readEntity(GameConversationDto.class).getGameHistoryId()).isEqualTo(gameHistoryId);
    }

    @Test
    void whiteSideUserId() {
        int whiteSideUserId = this.uniqueData.userId();
        GameConversation gameConversation = this.uniqueBuilders.gameConversation().whiteSideUserId(whiteSideUserId)
                .build();
        this.gameConversationDao.persist(gameConversation);
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameConversation);
        assertThat(response.readEntity(GameConversationDto.class).getWhiteSideUserId()).isEqualTo(whiteSideUserId);
    }

    @Test
    void blackSideUserId() {
        int blackSideUserId = this.uniqueData.userId();
        GameConversation gameConversation = this.uniqueBuilders.gameConversation().blackSideUserId(blackSideUserId)
                .build();
        this.gameConversationDao.persist(gameConversation);
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameConversation);
        assertThat(response.readEntity(GameConversationDto.class).getBlackSideUserId()).isEqualTo(blackSideUserId);
    }

    @Test
    void messagesUserId() {
        int userIdOne = this.uniqueData.userId();
        int userIdTwo = this.uniqueData.userId();
        GameConversation gameConversation = this.uniqueBuilders.gameConversation()
                .message(this.builders.message().from(userIdOne).build())
                .message(this.builders.message().from(userIdTwo).build()).build();
        this.gameConversationDao.persist(gameConversation);
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameConversation);
        List<MessageDto> messages = response.readEntity(GameConversationDto.class).getMessages();
        assertThat(messages.stream().map(MessageDto::getUserId).collect(Collectors.toList()))
                .containsExactly(userIdOne, userIdTwo).inOrder();
    }

    @Test
    void messagesText() {
        GameConversation gameConversation = this.uniqueBuilders.gameConversation()
                .message(this.builders.message().text("first game message text").build())
                .message(this.builders.message().text("second game message text").build()).build();
        this.gameConversationDao.persist(gameConversation);
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameConversation);
        List<MessageDto> messages = response.readEntity(GameConversationDto.class).getMessages();
        assertThat(messages.stream().map(MessageDto::getText).collect(Collectors.toList()))
                .containsExactly("first game message text", "second game message text").inOrder();
    }

    @Test
    void messagesEpochSeconds() {
        GameConversation gameConversation = this.uniqueBuilders.gameConversation()
                .message(this.builders.message().epochSeconds(7000L).build())
                .message(this.builders.message().epochSeconds(8000L).build()).build();
        this.gameConversationDao.persist(gameConversation);
        String token = this.userSecurity.tokenFor(gameConversation.getWhiteSideUserId());
        Response response = this.gameConversationClient.findByGameHistoryId(token, gameConversation);
        List<MessageDto> messages = response.readEntity(GameConversationDto.class).getMessages();
        assertThat(messages.stream().map(MessageDto::getEpochSeconds).collect(Collectors.toList()))
                .containsExactly(7000L, 8000L).inOrder();
    }
}
