package com.gitlab.akluball.chessapp.messaging.itest.data;

import com.gitlab.akluball.chessapp.messaging.itest.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.itest.transfer.GameHistoryDto;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.stream.Stream;

public class GameConversationDao {
    private final EntityManager entityManager;

    @Inject
    GameConversationDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public GameConversation findByGameHistoryId(GameHistoryDto gameHistoryDto) {
        return this.entityManager.find(GameConversation.class, gameHistoryDto.getId());
    }

    public void persist(GameConversation... gameConversations) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        Stream.of(gameConversations).forEach(this.entityManager::persist);
        tx.commit();
    }

    public void refresh(GameConversation gameConversation) {
        this.entityManager.refresh(gameConversation);
    }
}
