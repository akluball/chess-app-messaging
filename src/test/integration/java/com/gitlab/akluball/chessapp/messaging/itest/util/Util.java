package com.gitlab.akluball.chessapp.messaging.itest.util;

import com.google.common.collect.Range;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.stream.Collectors;

public class Util {
    public static final Range<Long> bufferBelow4(long endRange) {
        return Range.closed(endRange - 4, endRange);
    }

    public static RuntimeException asRuntime(Throwable throwable) {
        return new RuntimeException(throwable);
    }

    public static void sleepMillis(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw asRuntime(e);
        }
    }

    @SafeVarargs
    public static <E> E[] concat(Class<E> klass, E[]... arraysToConcat) {
        int concatLength = 0;
        for (E[] arrayToConcat : arraysToConcat) {
            concatLength += arrayToConcat.length;
        }
        @SuppressWarnings("unchecked")
        E[] concat = (E[]) Array.newInstance(klass, concatLength);
        int concatIndex = 0;
        for (E[] arrayToConcat : arraysToConcat) {
            for (E element : arrayToConcat) {
                concat[concatIndex] = element;
                concatIndex++;
            }
        }
        return concat;
    }

    public static String inputStreamToString(InputStream inputStream) {
        return new BufferedReader(new InputStreamReader(inputStream))
                .lines()
                .collect(Collectors.joining(System.lineSeparator()));
    }
}
