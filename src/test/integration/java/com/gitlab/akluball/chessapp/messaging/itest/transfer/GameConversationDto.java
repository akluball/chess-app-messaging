package com.gitlab.akluball.chessapp.messaging.itest.transfer;

import java.util.List;

public class GameConversationDto {
    private int gameHistoryId;
    private int whiteSideUserId;
    private int blackSideUserId;
    private List<MessageDto> messages;

    public int getGameHistoryId() {
        return gameHistoryId;
    }

    public int getWhiteSideUserId() {
        return whiteSideUserId;
    }

    public int getBlackSideUserId() {
        return blackSideUserId;
    }

    public List<MessageDto> getMessages() {
        return messages;
    }
}
