package com.gitlab.akluball.chessapp.messaging.client;

import com.gitlab.akluball.chessapp.messaging.json.JsonbContextResolver;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

@Singleton
public class ClientProducer {
    private final Client client;

    @Inject
    ClientProducer(JsonbContextResolver jsonbContextResolver) {
        this.client = ClientBuilder.newClient()
                .register(jsonbContextResolver);
    }

    @Produces
    public Client client() {
        return this.client;
    }
}
