package com.gitlab.akluball.chessapp.messaging;

import com.gitlab.akluball.chessapp.messaging.controller.openapi.OpenApiSecuritySchemes;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
@OpenAPIDefinition
@SecurityScheme(
        type = SecuritySchemeType.HTTP,
        in = SecuritySchemeIn.HEADER,
        name = OpenApiSecuritySchemes.BEARER,
        scheme = "bearer",
        description = "Bearer token authentication"
)
public class MessagingApplication extends Application {
}
