package com.gitlab.akluball.chessapp.messaging.security;

import com.gitlab.akluball.chessapp.messaging.exception.Http401;
import com.nimbusds.jwt.SignedJWT;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Provider
@RequestScoped
@Bearer
public class BearerFilter implements ContainerRequestFilter {
    @Context
    private ResourceInfo resourceInfo;
    private UserSecurity userSecurity;
    private int currentUserId;

    public BearerFilter() {
    }

    @Inject
    public BearerFilter(UserSecurity userSecurity) {
        this.userSecurity = userSecurity;
    }

    private static List<SignedJWT> extractTokens(List<String> authorizationHeaders) {
        return authorizationHeaders.stream()
                .filter(header -> header.startsWith("Bearer "))
                .map(header -> header.replace("Bearer ", ""))
                .map(serialized -> {
                    try {
                        return SignedJWT.parse(serialized);
                    } catch (ParseException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private static Map<BearerRole, SignedJWT> buildRoleToToken(List<SignedJWT> tokens) {
        Map<BearerRole, SignedJWT> roleToToken = new HashMap<>();
        for (SignedJWT token : tokens) {
            try {
                List<String> roles = token.getJWTClaimsSet().getStringListClaim(BearerRole.CLAIM_NAME);
                roles.stream()
                        .map(BearerRole::fromString)
                        .filter(Objects::nonNull)
                        .forEach(br -> roleToToken.put(br, token));
            } catch (ParseException e) { // ignore bad tokens
            }
        }
        return roleToToken;
    }

    @Produces
    @CurrentUserId
    public int currentUserId() {
        return this.currentUserId;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        List<String> authorizationHeaders = requestContext.getHeaders().get("Authorization");
        if (authorizationHeaders == null || authorizationHeaders.isEmpty()) {
            throw new Http401();
        }
        List<SignedJWT> tokens = extractTokens(authorizationHeaders);
        Map<BearerRole, SignedJWT> roleToToken = buildRoleToToken(tokens);
        Bearer bearerAnnotation = this.resourceInfo.getResourceMethod().getAnnotation(Bearer.class);
        Set<BearerRole> allowedRoles = Stream.of(bearerAnnotation.roles()).collect(Collectors.toSet());
        if (allowedRoles.contains(BearerRole.USER)) {
            SignedJWT validToken = roleToToken.entrySet().stream()
                    .filter(e -> e.getKey().equals(BearerRole.USER))
                    .map(Map.Entry::getValue)
                    .filter(SecurityUtil::isNotExpired)
                    .filter(this.userSecurity::isValidSignature)
                    .findFirst()
                    .orElse(null);
            if (validToken != null) {
                try {
                    this.currentUserId = Integer.parseInt(validToken.getJWTClaimsSet().getSubject());
                    // success - user auth
                    return;
                } catch (ParseException e) { // ignore bad token
                }
            }
        }
        // otherwise fail
        throw new Http401();
    }
}
