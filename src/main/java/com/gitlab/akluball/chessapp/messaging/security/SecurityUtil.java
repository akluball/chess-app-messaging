package com.gitlab.akluball.chessapp.messaging.security;

import com.nimbusds.jwt.SignedJWT;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

import static com.gitlab.akluball.chessapp.messaging.util.Util.asRuntime;

public class SecurityUtil {
    public static final Pattern BEARER_TOKEN_QUERY_PATTERN = Pattern.compile("&?bearer=([^&]+)&?");

    public static SignedJWT parseSignedToken(String serializedToken) {
        try {
            return SignedJWT.parse(serializedToken);
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }

    public static String bearer(String serializedToken) {
        return String.format("Bearer %s", serializedToken);
    }

    public static boolean isExpired(SignedJWT token) {
        try {
            Date expiration = token.getJWTClaimsSet().getExpirationTime();
            return Objects.isNull(expiration) || !expiration.toInstant().isAfter(Instant.now());
        } catch (ParseException e) {
            return true;
        }
    }

    public static boolean isNotExpired(SignedJWT token) {
        return !isExpired(token);
    }
}
