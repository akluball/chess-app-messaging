package com.gitlab.akluball.chessapp.messaging.data;

import com.gitlab.akluball.chessapp.messaging.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.model.Converser;
import com.gitlab.akluball.chessapp.messaging.model.Message;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class ConversationDao {

    @PersistenceContext(unitName = "chessMessagingPu")
    private EntityManager entityManager;

    public Conversation createConversation(Set<Converser> conversers, Message initialMessage) {
        Conversation conversation = new Conversation.Builder().message(initialMessage)
                .conversers(conversers)
                .build();
        this.entityManager.persist(conversation);
        return conversation;
    }

    public Conversation findById(int conversationId) {
        return this.entityManager.find(Conversation.class, conversationId);
    }

    public List<Conversation> findInvolvingUser(int userId) {
        return this.entityManager
                .createNamedQuery(Converser.Query.Name.FIND_CONVERSATIONS_BY_USER_ID, Object[].class)
                .setParameter(Converser.Query.Param.USER_ID, userId)
                .getResultList()
                .stream()
                .map(result -> result[0])
                .map(Conversation.class::cast)
                .collect(Collectors.toList());
    }

    public Conversation findByConversers(Set<Converser> conversers) {
        if (conversers.isEmpty()) {
            return null;
        } else {
            return conversers.iterator()
                    .next()
                    .getConversationBetweenExactly(conversers);
        }
    }

    public Conversation findConversationBetween(Set<Integer> userIds) {
        if (userIds.isEmpty()) {
            return null;
        }
        return this.entityManager
                .createNamedQuery(Converser.Query.Name.FIND_CONVERSATION_BY_USER_IDS, Conversation.class)
                .setParameter(Converser.Query.Param.USER_ID, userIds.iterator().next())
                .getResultStream()
                .filter((conversation) -> conversation.conversersAreExactly(userIds))
                .findFirst()
                .orElse(null);
    }
}
