package com.gitlab.akluball.chessapp.messaging.service;

import com.gitlab.akluball.chessapp.messaging.data.ConversationDao;
import com.gitlab.akluball.chessapp.messaging.data.ConverserDao;
import com.gitlab.akluball.chessapp.messaging.exception.Http400;
import com.gitlab.akluball.chessapp.messaging.exception.Http401;
import com.gitlab.akluball.chessapp.messaging.exception.Http403;
import com.gitlab.akluball.chessapp.messaging.exception.Http404;
import com.gitlab.akluball.chessapp.messaging.exception.Http409;
import com.gitlab.akluball.chessapp.messaging.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.model.Converser;
import com.gitlab.akluball.chessapp.messaging.model.Message;
import com.gitlab.akluball.chessapp.messaging.transfer.ConversationInitializerDto;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class ConversationService {

    private ConversationDao conversationDao;
    private ConverserDao converserDao;

    public ConversationService() {
    }

    @Inject
    public ConversationService(ConversationDao conversationDao, ConverserDao converserDao) {
        this.conversationDao = conversationDao;
        this.converserDao = converserDao;
    }

    public Conversation create(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "ConversationInitializer is required") @Valid ConversationInitializerDto initializer
    ) {
        if (!initializer.isConverser(currentUserId)) {
            throw new Http403();
        }
        if (initializer.isSenderRecipient()) {
            throw new Http400("senderUserId should not be in recipientUserIds");
        }
        Set<Converser> conversers = initializer.toConversers(this.converserDao);
        if (this.conversationDao.findByConversers(conversers) != null) {
            String converserIdsRepr = conversers.stream()
                    .map(Converser::getUserId)
                    .map(Object::toString)
                    .collect(Collectors.joining(", "));
            throw new Http409(String.format("conversation with conversers: %s", converserIdsRepr));
        }
        return this.conversationDao.createConversation(conversers, initializer.toInitialMessage());
    }

    public Conversation findById(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "conversationId must be an integer") Integer conversationId
    ) {
        Conversation conversation = this.conversationDao.findById(conversationId);
        if (conversation == null) {
            throw new Http404(String.format("Conversation with id %s", conversationId));
        }
        if (conversation.isNotConverser(currentUserId)) {
            throw new Http403();
        }
        return conversation;
    }

    public List<Conversation> findInvolvingCurrentUser(@NotNull(payload = Http401.Payload.class) Integer currentUserId) {
        return this.conversationDao.findInvolvingUser(currentUserId);
    }

    public Conversation findConversationBetween(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotEmpty(message = "userId is required") Set<@NotNull(message = "userId must be non-null") Integer> userIds
    ) {
        Conversation conversation = this.conversationDao.findConversationBetween(userIds);
        if (Objects.isNull(conversation)) {
            String ids = userIds.stream().map(Object::toString).collect(Collectors.joining(", "));
            throw new Http404(String.format("Conversation with participants [ %s ]", ids));
        }
        if (conversation.isNotConverser(currentUserId)) {
            throw new Http403();
        }
        return conversation;
    }

    public void pushMessage(
            @NotNull(message = "conversationId is required") Integer conversationId,
            @NotNull(message = "Message is required") @Valid Message message
    ) {
        Conversation conversation = this.conversationDao.findById(conversationId);
        if (Objects.nonNull(conversation)) {
            conversation.pushMessage(message);
        }
    }
}
