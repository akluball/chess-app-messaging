package com.gitlab.akluball.chessapp.messaging.exception;

public class Http404 extends HttpException {
    public Http404(String criteria) {
        super(404, String.format("not found: %s", criteria));
    }
}
