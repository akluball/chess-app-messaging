package com.gitlab.akluball.chessapp.messaging.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Embeddable
@Access(AccessType.FIELD)
public class Message {
    @NotNull(message = "Message.userId is required")
    private Integer userId;
    @NotBlank(message = "Message.text is required")
    private String text;
    @NotNull(message = "Message.epochSeconds is required")
    private Long epochSeconds;

    public Dto toDto() {
        return this.new Dto();
    }

    public static class Builder {
        private final Message message;

        public Builder() {
            this.message = new Message();
        }

        public Builder sender(int userId) {
            this.message.userId = userId;
            return this;
        }

        public Builder text(String text) {
            this.message.text = text;
            return this;
        }

        public Message build() {
            this.message.epochSeconds = Instant.now().getEpochSecond();
            return this.message;
        }
    }

    @Schema(name = "Message")
    public class Dto {
        private int userId;
        private String text;
        private long epochSeconds;

        private Dto() {
            Message message = Message.this;
            this.userId = message.userId;
            this.text = message.text;
            this.epochSeconds = message.epochSeconds;
        }
    }
}
