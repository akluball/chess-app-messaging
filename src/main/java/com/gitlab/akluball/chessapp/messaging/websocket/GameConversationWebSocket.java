package com.gitlab.akluball.chessapp.messaging.websocket;

import com.gitlab.akluball.chessapp.messaging.exception.Http401;
import com.gitlab.akluball.chessapp.messaging.exception.HttpException;
import com.gitlab.akluball.chessapp.messaging.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.model.Message;
import com.gitlab.akluball.chessapp.messaging.security.SecurityUtil;
import com.gitlab.akluball.chessapp.messaging.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.service.GameConversationService;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;

import static com.gitlab.akluball.chessapp.messaging.security.SecurityUtil.isExpired;
import static com.gitlab.akluball.chessapp.messaging.security.SecurityUtil.parseSignedToken;
import static com.gitlab.akluball.chessapp.messaging.util.Util.asRuntime;
import static com.gitlab.akluball.chessapp.messaging.util.Util.getHttpExceptionCause;

@ServerEndpoint(
        value = "/api/gameconversation/{gameHistoryId}",
        encoders = MessageDtoEncoder.class
)
public class GameConversationWebSocket {
    private static final String USER_ID_PARAM = "userIdParam";

    private GameConversationWebSocketMesh mesh;
    private UserSecurity userSecurity;
    private GameConversationService gameConversationService;

    public GameConversationWebSocket() {
    }

    @Inject
    public GameConversationWebSocket(GameConversationWebSocketMesh gameConversationWebSocketMesh,
            UserSecurity userSecurity,
            GameConversationService gameConversationService) {
        this.mesh = gameConversationWebSocketMesh;
        this.userSecurity = userSecurity;
        this.gameConversationService = gameConversationService;
    }

    private void unexpectedCondition(Session session, String reasonPhrase) {
        try {
            session.close(new CloseReason(CloseCodes.UNEXPECTED_CONDITION, reasonPhrase));
        } catch (IOException e) {
            throw asRuntime(e);
        }
    }

    @OnOpen
    public void authenticate(Session session, @PathParam("gameHistoryId") int gameHistoryId) throws IOException {
        String queryString = session.getQueryString();
        queryString = (queryString == null) ? "" : queryString;
        Matcher tokenMatcher = SecurityUtil.BEARER_TOKEN_QUERY_PATTERN.matcher(queryString);
        if (!tokenMatcher.find()) {
            throw new Http401();
        }
        SignedJWT token = parseSignedToken(tokenMatcher.group(1));
        if (isExpired(token)) {
            throw new Http401();
        }
        if (!this.userSecurity.isUserRole(token)) {
            throw new Http401();
        }
        if (!this.userSecurity.isValidSignature(token)) {
            throw new Http401();
        }
        int userId = this.userSecurity.getUserId(token);
        GameConversation gameConversation = this.gameConversationService.findById(userId, gameHistoryId);
        session.getUserProperties().put(USER_ID_PARAM, userId);
        this.mesh.register(gameHistoryId, session);
    }

    @OnError
    public void handleError(Session session, Throwable throwable) {
        HttpException httpException = getHttpExceptionCause(throwable);
        if (httpException != null) {
            this.unexpectedCondition(session, httpException.getReason());
        } else {
            this.unexpectedCondition(session, "server error");
            throw asRuntime(throwable);
        }
    }

    @OnMessage
    public void handleMessage(Session session, @PathParam("gameHistoryId") Integer gameHistoryId, String text) {
        if (Objects.isNull(text) || text.isEmpty()) {
            return;
        }
        int userId = (int) session.getUserProperties().get(USER_ID_PARAM);
        Message message = new Message.Builder().sender(userId).text(text).build();
        this.gameConversationService.pushMessage(gameHistoryId, message);
        this.mesh.broadcast(gameHistoryId, message.toDto());
    }

    @OnClose
    public void unregister(Session session, @PathParam("gameHistoryId") int gameHistoryId) {
        this.mesh.unregister(gameHistoryId, session);
    }
}
