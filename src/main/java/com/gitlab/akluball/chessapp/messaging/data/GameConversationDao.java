package com.gitlab.akluball.chessapp.messaging.data;

import com.gitlab.akluball.chessapp.messaging.model.GameConversation;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class GameConversationDao {

    @PersistenceContext(unitName = "chessMessagingPu")
    private EntityManager entityManager;

    public GameConversation create(GameConversation.GameHistoryDto gameHistoryDto) {
        GameConversation gameConversation = gameHistoryDto.toGameConversation();
        this.entityManager.persist(gameConversation);
        return gameConversation;
    }

    public GameConversation findById(int gameHistoryId) {
        return this.entityManager.find(GameConversation.class, gameHistoryId);
    }
}
