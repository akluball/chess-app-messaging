package com.gitlab.akluball.chessapp.messaging.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OrderColumn;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Access(AccessType.FIELD)
public class GameConversation {
    @Id
    private int gameHistoryId;
    private int whiteSideUserId;
    private int blackSideUserId;
    @ElementCollection
    @OrderColumn
    private List<Message> messages = new ArrayList<>();

    public boolean isParticipant(int userId) {
        return this.whiteSideUserId == userId || this.blackSideUserId == userId;
    }

    public void pushMessage(Message message) {
        this.messages.add(message);
    }

    public Dto toDto() {
        return this.new Dto();
    }

    public static class GameHistoryDto {
        private int id;
        private int whiteSideUserId;
        private int blackSideUserId;

        public GameConversation toGameConversation() {
            GameConversation gameConversation = new GameConversation();
            gameConversation.gameHistoryId = this.id;
            gameConversation.whiteSideUserId = this.whiteSideUserId;
            gameConversation.blackSideUserId = this.blackSideUserId;
            return gameConversation;
        }
    }

    @Schema(name = "GameConversation")
    public class Dto {
        private int gameHistoryId;
        private int whiteSideUserId;
        private int blackSideUserId;
        private List<Message.Dto> messages;

        private Dto() {
            GameConversation gameConversation = GameConversation.this;
            this.gameHistoryId = gameConversation.gameHistoryId;
            this.whiteSideUserId = gameConversation.whiteSideUserId;
            this.blackSideUserId = gameConversation.blackSideUserId;
            this.messages = gameConversation.messages
                    .stream()
                    .map(Message::toDto)
                    .collect(Collectors.toList());
        }
    }
}
