package com.gitlab.akluball.chessapp.messaging.controller;

import com.gitlab.akluball.chessapp.messaging.controller.openapi.OpenApiSecuritySchemes;
import com.gitlab.akluball.chessapp.messaging.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.messaging.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.security.Bearer;
import com.gitlab.akluball.chessapp.messaging.security.BearerRole;
import com.gitlab.akluball.chessapp.messaging.security.CurrentUserId;
import com.gitlab.akluball.chessapp.messaging.service.GameConversationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("gameconversation")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.GAME_CONVERSATION)
@SecurityRequirement(name = OpenApiSecuritySchemes.BEARER)
public class GameConversationController {

    private GameConversationService gameConversationService;
    private Integer currentUserId;

    public GameConversationController() {
    }

    @Inject
    public GameConversationController(
            GameConversationService gameConversationService,
            @CurrentUserId Integer currentUserId
    ) {
        this.gameConversationService = gameConversationService;
        this.currentUserId = currentUserId;
    }

    @GET
    @Path("{gameHistoryId}")
    @Bearer(roles = { BearerRole.USER })
    @Operation(
            summary = "Gets game conversation by game history id",
            description = "Gets game conversation corresponding to given game history id"
    )
    @ApiResponse(
            responseCode = "200",
            description = "Fetched game conversation",
            content = @Content(schema = @Schema(implementation = GameConversation.Dto.class))
    )
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "404", description = "not found")
    public GameConversation.Dto findById(@PathParam("gameHistoryId") Integer gameHistoryId) {
        return this.gameConversationService.findById(this.currentUserId, gameHistoryId).toDto();
    }
}
