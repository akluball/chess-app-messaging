package com.gitlab.akluball.chessapp.messaging.security;

public enum ServiceName {
    MESSAGING("messaging");

    private final String asString;

    ServiceName(String asString) {
        this.asString = asString;
    }

    public String asString() {
        return this.asString;
    }
}
