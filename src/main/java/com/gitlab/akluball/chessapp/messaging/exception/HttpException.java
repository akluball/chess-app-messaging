package com.gitlab.akluball.chessapp.messaging.exception;

import javax.ejb.ApplicationException;
import javax.ws.rs.WebApplicationException;

@ApplicationException(inherited = true, rollback = true)
public class HttpException extends WebApplicationException {
    private final int statusCode;
    private final String reason;

    public HttpException(int statusCode, String reason) {
        this.statusCode = statusCode;
        this.reason = reason;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getReason() {
        return reason;
    }
}
