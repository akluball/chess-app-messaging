package com.gitlab.akluball.chessapp.messaging.security;

import com.gitlab.akluball.chessapp.messaging.MessagingConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.ParseException;
import java.util.List;

import static com.gitlab.akluball.chessapp.messaging.util.Util.asRuntime;

@Singleton
public class UserSecurity {
    private RSASSAVerifier verifier;

    @Inject
    UserSecurity(MessagingConfig messagingConfig) {
        this.verifier = new RSASSAVerifier(messagingConfig.userPublicKey());
    }

    public boolean isUserRole(SignedJWT token) {
        try {
            List<String> roles = token.getJWTClaimsSet().getStringListClaim(BearerRole.CLAIM_NAME);
            return roles.contains(BearerRole.USER.asString());
        } catch (ParseException e) {
            return false;
        }
    }

    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            return false;
        }
    }

    public int getUserId(SignedJWT token) {
        try {
            return Integer.parseInt(token.getJWTClaimsSet().getSubject());
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }
}
