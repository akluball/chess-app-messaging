package com.gitlab.akluball.chessapp.messaging.websocket;

import com.gitlab.akluball.chessapp.messaging.exception.Http401;
import com.gitlab.akluball.chessapp.messaging.exception.Http403;
import com.gitlab.akluball.chessapp.messaging.exception.Http404;
import com.gitlab.akluball.chessapp.messaging.exception.HttpException;
import com.gitlab.akluball.chessapp.messaging.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.model.Message;
import com.gitlab.akluball.chessapp.messaging.security.SecurityUtil;
import com.gitlab.akluball.chessapp.messaging.security.UserSecurity;
import com.gitlab.akluball.chessapp.messaging.service.ConversationService;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;

import static com.gitlab.akluball.chessapp.messaging.security.SecurityUtil.isExpired;
import static com.gitlab.akluball.chessapp.messaging.security.SecurityUtil.parseSignedToken;
import static com.gitlab.akluball.chessapp.messaging.util.Util.asRuntime;
import static com.gitlab.akluball.chessapp.messaging.util.Util.getHttpExceptionCause;

@ServerEndpoint(
        value = "/api/conversation/{conversationId}",
        encoders = { MessageDtoEncoder.class }
)
public class ConversationWebSocket {
    private static final String USER_ID_PARAM = "userIdParam";

    private UserSecurity userSecurity;
    private ConversationService conversationService;
    private ConversationWebSocketMesh endpointMesh;

    public ConversationWebSocket() {
    }

    @Inject
    public ConversationWebSocket(UserSecurity userSecurity,
            ConversationService conversationService,
            ConversationWebSocketMesh mesh) {
        this.userSecurity = userSecurity;
        this.conversationService = conversationService;
        this.endpointMesh = mesh;
    }

    private void unexpectedCondition(Session session, String reasonPhrase) {
        try {
            session.close(new CloseReason(CloseCodes.UNEXPECTED_CONDITION, reasonPhrase));
        } catch (IOException e) {
            throw asRuntime(e);
        }
    }

    @OnOpen
    public void authenticate(Session session, @PathParam("conversationId") int conversationId) {
        String queryString = session.getQueryString();
        queryString = (queryString == null) ? "" : queryString;
        Matcher tokenMatcher = SecurityUtil.BEARER_TOKEN_QUERY_PATTERN.matcher(queryString);
        if (!tokenMatcher.find()) {
            throw new Http401();
        }
        SignedJWT token = parseSignedToken(tokenMatcher.group(1));
        if (isExpired(token)) {
            throw new Http401();
        }
        if (!this.userSecurity.isUserRole(token)) {
            throw new Http401();
        }
        if (!this.userSecurity.isValidSignature(token)) {
            throw new Http401();
        }
        int userId = this.userSecurity.getUserId(token);
        session.getUserProperties().put(USER_ID_PARAM, userId);
        Conversation conversation = this.conversationService.findById(userId, conversationId);
        if (conversation == null) {
            throw new Http404(String.format("Conversation with id: %s", conversationId));
        }
        if (conversation.isNotConverser(userId)) {
            throw new Http403();
        } else {
            this.endpointMesh.register(conversationId, session);
        }
    }

    @OnError
    public void handleError(Session session, Throwable throwable) {
        HttpException httpException = getHttpExceptionCause(throwable);
        if (httpException != null) {
            this.unexpectedCondition(session, httpException.getReason());
        } else {
            this.unexpectedCondition(session, "server error");
            throw asRuntime(throwable);
        }
    }

    @OnMessage
    public void handleMessage(Session session, @PathParam("conversationId") Integer conversationId, String text) {
        if (Objects.isNull(text) || text.isEmpty()) {
            return;
        }
        int userId = (int) session.getUserProperties().get(USER_ID_PARAM);
        Message message = new Message.Builder().sender(userId).text(text).build();
        this.conversationService.pushMessage(conversationId, message);
        this.endpointMesh.broadcast(conversationId, message.toDto());
    }

    @OnClose
    public void unregister(Session session, @PathParam("conversationId") int conversationId) {
        this.endpointMesh.unregister(conversationId, session);
    }
}
