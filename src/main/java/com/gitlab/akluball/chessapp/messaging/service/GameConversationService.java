package com.gitlab.akluball.chessapp.messaging.service;

import com.gitlab.akluball.chessapp.messaging.client.GameHistoryClient;
import com.gitlab.akluball.chessapp.messaging.data.GameConversationDao;
import com.gitlab.akluball.chessapp.messaging.exception.Http401;
import com.gitlab.akluball.chessapp.messaging.exception.Http403;
import com.gitlab.akluball.chessapp.messaging.exception.Http404;
import com.gitlab.akluball.chessapp.messaging.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.model.Message;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Stateless
public class GameConversationService {

    private GameConversationDao gameConversationDao;
    private GameHistoryClient gameHistoryClient;

    public GameConversationService() {
    }

    @Inject
    public GameConversationService(GameConversationDao gameConversationDao, GameHistoryClient gameHistoryClient) {
        this.gameConversationDao = gameConversationDao;
        this.gameHistoryClient = gameHistoryClient;
    }

    private GameConversation create(int gameHistoryId) {
        GameConversation.GameHistoryDto gameHistoryDto = this.gameHistoryClient.getGameHistory(gameHistoryId);
        if (Objects.isNull(gameHistoryDto)) {
            throw new Http404(String.format("GameHistory with id %s", gameHistoryId));
        }
        return this.gameConversationDao.create(gameHistoryDto);
    }

    public GameConversation findById(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotNull(message = "gameHistoryId must be an integer") Integer gameHistoryId
    ) {
        GameConversation gameConversation = this.gameConversationDao.findById(gameHistoryId);
        if (gameConversation == null) {
            gameConversation = this.create(gameHistoryId);
        }
        if (!gameConversation.isParticipant(currentUserId)) {
            throw new Http403();
        }
        return gameConversation;
    }

    public void pushMessage(
            @NotNull(message = "gameHistoryId is required") Integer gameHistoryId,
            @NotNull(message = "Message is required") @Valid Message message
    ) {
        GameConversation gameConversation = this.gameConversationDao.findById(gameHistoryId);
        gameConversation.pushMessage(message);
    }
}
