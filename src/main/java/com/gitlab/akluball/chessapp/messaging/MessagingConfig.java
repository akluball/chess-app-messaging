package com.gitlab.akluball.chessapp.messaging;

import javax.inject.Singleton;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static com.gitlab.akluball.chessapp.messaging.util.Util.minutesToSeconds;

@Singleton
public class MessagingConfig {
    private final String gameHistoryUri;
    private final PrivateKey messagingPrivateKey;
    private final RSAPublicKey userPublicKey;

    MessagingConfig() throws NoSuchAlgorithmException, InvalidKeySpecException {
        this.gameHistoryUri = System.getenv("CHESSAPP_GAMEHISTORY_URI");
        Base64.Decoder decoder = Base64.getDecoder();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        byte[] messagingPrivateKeyBytes = decoder.decode(System.getenv("CHESSAPP_MESSAGING_PRIVATEKEY"));
        this.messagingPrivateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(messagingPrivateKeyBytes));
        byte[] userPublicKeyBytes = decoder.decode(System.getenv("CHESSAPP_USER_PUBLICKEY"));
        this.userPublicKey = (RSAPublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(userPublicKeyBytes));
    }

    public String gameHistoryUri() {
        return this.gameHistoryUri;
    }

    public PrivateKey messagingPrivateKey() {
        return this.messagingPrivateKey;
    }

    public int tokenLifeSeconds() {
        return minutesToSeconds(5);
    }

    public RSAPublicKey userPublicKey() {
        return this.userPublicKey;
    }
}
