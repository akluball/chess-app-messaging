package com.gitlab.akluball.chessapp.messaging.json;

import javax.enterprise.inject.Produces;
import javax.inject.Singleton;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;

@Singleton
public class JsonbProducer {
    private final Jsonb jsonb;

    JsonbProducer() {
        JsonbConfig config = new JsonbConfig().withPropertyVisibilityStrategy(new FieldVisibilityStrategy());
        this.jsonb = JsonbBuilder.create(config);
    }

    @Produces
    public Jsonb jsonb() {
        return this.jsonb;
    }
}
