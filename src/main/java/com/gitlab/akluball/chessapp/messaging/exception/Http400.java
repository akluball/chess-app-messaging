package com.gitlab.akluball.chessapp.messaging.exception;

public class Http400 extends HttpException {
    public Http400() {
        super(400, "bad request");
    }

    public Http400(String reasonPhrase) {
        super(400, String.format("bad request: %s", reasonPhrase));
    }
}
