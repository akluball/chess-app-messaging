package com.gitlab.akluball.chessapp.messaging.controller.openapi;

public class OpenApiTags {
    public static final String CONVERSATION = "Conversation";
    public static final String GAME_CONVERSATION = "Game Conversation";
}
