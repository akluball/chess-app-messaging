package com.gitlab.akluball.chessapp.messaging.security;

import com.gitlab.akluball.chessapp.messaging.MessagingConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.gitlab.akluball.chessapp.messaging.util.Util.asRuntime;

@Singleton
public class MessagingSecurity {
    private final RSASSASigner signer;
    private final int tokenLifeSeconds;
    private Instant expiration;
    private SignedJWT token;

    @Inject
    MessagingSecurity(MessagingConfig messagingConfig) {
        this.signer = new RSASSASigner(messagingConfig.messagingPrivateKey());
        this.tokenLifeSeconds = messagingConfig.tokenLifeSeconds();
    }

    private boolean requiresRefresh() {
        return Objects.isNull(this.token) || this.expiration.isBefore(Instant.now().plusSeconds(5));
    }

    private void refresh() {
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        this.expiration = Instant.now().plusSeconds(this.tokenLifeSeconds);
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject("" + ServiceName.MESSAGING.asString())
                .claim(BearerRole.CLAIM_NAME, Stream.of(BearerRole.SERVICE.asString()).collect(Collectors.toList()))
                .expirationTime(Date.from(this.expiration))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        try {
            token.sign(this.signer);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        this.token = token;
    }

    public String getToken() {
        if (this.requiresRefresh()) {
            this.refresh();
        }
        return this.token.serialize();
    }
}
