package com.gitlab.akluball.chessapp.messaging.controller;

import com.gitlab.akluball.chessapp.messaging.controller.openapi.OpenApiSecuritySchemes;
import com.gitlab.akluball.chessapp.messaging.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.messaging.model.Conversation;
import com.gitlab.akluball.chessapp.messaging.security.Bearer;
import com.gitlab.akluball.chessapp.messaging.security.BearerRole;
import com.gitlab.akluball.chessapp.messaging.security.CurrentUserId;
import com.gitlab.akluball.chessapp.messaging.service.ConversationService;
import com.gitlab.akluball.chessapp.messaging.transfer.ConversationInitializerDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Path("conversation")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.CONVERSATION)
@SecurityRequirement(name = OpenApiSecuritySchemes.BEARER)
public class ConversationController {

    private ConversationService conversationService;
    private Integer currentUserId;

    public ConversationController() {
    }

    @Inject
    public ConversationController(ConversationService conversationService, @CurrentUserId Integer currentUserId) {
        this.conversationService = conversationService;
        this.currentUserId = currentUserId;
    }

    @POST
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Creates conversation", description = "Creates conversation between users")
    @ApiResponse(
            responseCode = "200",
            description = "Created conversation",
            content = @Content(schema = @Schema(implementation = Conversation.Dto.class))
    )
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "409", description = "existing conversation between users")
    public Conversation.Dto create(ConversationInitializerDto initializer) {
        return this.conversationService.create(this.currentUserId, initializer).toDto();
    }

    @GET
    @Path("{conversationId}")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Gets conversation by id", description = "Gets conversation with the given conversation id")
    @ApiResponse(
            responseCode = "200",
            description = "Fetched conversation",
            content = @Content(schema = @Schema(implementation = Conversation.Dto.class))
    )
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "404", description = "not found")
    public Conversation.Dto getConversation(@PathParam("conversationId") Integer conversationId) {
        return this.conversationService.findById(this.currentUserId, conversationId).toDto();
    }

    @GET
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Gets conversations by user id", description = "Gets conversations involving the user")
    @ApiResponse(
            responseCode = "200",
            description = "Fetched conversations",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = Conversation.Dto.class)))
    )
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public List<Conversation.Dto> findInvolvingCurrentUser() {
        return this.conversationService.findInvolvingCurrentUser(this.currentUserId)
                .stream()
                .map(Conversation::toDto)
                .collect(Collectors.toList());
    }

    @GET
    @Path("between")
    @Bearer(roles = { BearerRole.USER })
    @Operation(summary = "Gets conversation between users", description = "Gets conversations between the given users")
    @ApiResponse(
            responseCode = "200",
            description = "Fetched conversations",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = Conversation.Dto.class)))
    )
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    @ApiResponse(responseCode = "403", description = "not authorized")
    @ApiResponse(responseCode = "404", description = "not found")
    public Conversation.Dto findConversationBetween(@QueryParam("userId") Set<Integer> userIds) {
        return this.conversationService.findConversationBetween(this.currentUserId, userIds).toDto();
    }
}
