package com.gitlab.akluball.chessapp.messaging.websocket;

import com.gitlab.akluball.chessapp.messaging.model.Message;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class MessageDtoEncoder implements Encoder.Text<Message.Dto> {
    private final Jsonb jsonb;

    @Inject
    MessageDtoEncoder(Jsonb jsonb) {
        this.jsonb = jsonb;
    }

    @Override
    public String encode(Message.Dto messageDto) throws EncodeException {
        return this.jsonb.toJson(messageDto);
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
