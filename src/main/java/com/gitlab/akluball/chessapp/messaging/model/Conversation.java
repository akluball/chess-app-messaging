package com.gitlab.akluball.chessapp.messaging.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Access(AccessType.FIELD)
public class Conversation {

    @Id
    @GeneratedValue
    private int id;
    @ManyToMany(mappedBy = "conversations")
    private Set<Converser> conversers = new HashSet<>();
    @ElementCollection
    @OrderColumn
    private List<Message> messages = new ArrayList<>();

    public void pushMessage(Message message) {
        this.messages.add(message);
    }

    public boolean isConversationBetweenExactly(Set<Converser> someConversers) {
        return this.conversers.equals(someConversers);
    }

    public boolean conversersAreExactly(Set<Integer> userIds) {
        if (userIds.size() != this.conversers.size()) {
            return false;
        }
        return this.conversers.stream()
                .allMatch(converser -> converser.userIdIsOneOf(userIds));
    }

    public boolean isNotConverser(int userId) {
        return this.conversers.stream()
                .noneMatch(c -> c.hasUserId(userId));
    }

    public Dto toDto() {
        return this.new Dto();
    }

    public static class Builder {
        private final Conversation conversation;

        public Builder() {
            this.conversation = new Conversation();
        }

        public Builder conversers(Set<Converser> conversers) {
            this.conversation.conversers.addAll(conversers);
            conversers.forEach(converser -> converser.addConversation(this.conversation));
            return this;
        }

        public Builder message(Message message) {
            this.conversation.messages.add(message);
            return this;
        }

        public Conversation build() {
            return this.conversation;
        }
    }

    @Schema(name = "Conversation")
    public class Dto {
        public List<Message.Dto> messages;
        private int id;
        private Set<Integer> converserIds;

        private Dto() {
            Conversation conversation = Conversation.this;
            this.id = conversation.id;
            this.converserIds = conversation.conversers.stream()
                    .map(Converser::getUserId)
                    .collect(Collectors.toSet());
            this.messages = conversation.messages.stream()
                    .map(Message::toDto)
                    .collect(Collectors.toList());
        }
    }
}
