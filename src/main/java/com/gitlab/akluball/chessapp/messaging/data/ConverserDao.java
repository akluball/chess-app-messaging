package com.gitlab.akluball.chessapp.messaging.data;

import com.gitlab.akluball.chessapp.messaging.model.Converser;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class ConverserDao {

    @PersistenceContext(unitName = "chessMessagingPu")
    private EntityManager entityManager;

    public Converser findOrCreate(int userId) {
        Converser converser = this.findByUserId(userId);
        if (converser == null) {
            converser = new Converser(userId);
            this.entityManager.persist(converser);
        }
        return converser;
    }

    private Converser findByUserId(int userId) {
        return this.entityManager.find(Converser.class, userId);
    }
}
