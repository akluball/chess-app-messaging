package com.gitlab.akluball.chessapp.messaging.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.util.HashSet;
import java.util.Set;

@Entity
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(
                name = Converser.Query.Name.FIND_CONVERSATIONS_BY_USER_ID,
                query = Converser.Query.FIND_CONVERSATIONS_BY_USER_ID
        ),
        @NamedQuery(
                name = Converser.Query.Name.FIND_CONVERSATION_BY_USER_IDS,
                query = Converser.Query.FIND_CONVERSATION_BY_USER_IDS
        )
})
public class Converser {
    @Id
    private int userId;
    @ManyToMany
    private Set<Conversation> conversations = new HashSet<>();

    public Converser() {
    }

    public Converser(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return this.userId;
    }

    public boolean hasUserId(int userIdToCheck) {
        return this.userId == userIdToCheck;
    }

    public void addConversation(Conversation conversation) {
        this.conversations.add(conversation);
    }

    public Conversation getConversationBetweenExactly(Set<Converser> conversers) {
        return this.conversations.stream()
                .filter((conversation) -> conversation.isConversationBetweenExactly(conversers))
                .findFirst()
                .orElse(null);
    }

    public boolean userIdIsOneOf(Set<Integer> userIds) {
        return userIds.contains(this.userId);
    }

    public static class Query {
        public static final String FIND_CONVERSATIONS_BY_USER_ID
                = "SELECT conversation, MAX(message.epochSeconds) AS lastMessageEpochSeconds"
                + " FROM Converser converser JOIN converser.conversations conversation JOIN conversation.messages message"
                + " WHERE converser.userId=:" + Param.USER_ID
                + " GROUP BY conversation"
                + " ORDER BY lastMessageEpochSeconds DESC";
        public static final String FIND_CONVERSATION_BY_USER_IDS
                = "SELECT conversation FROM Converser converser JOIN converser.conversations conversation"
                + " WHERE converser.userId=:" + Param.USER_ID;

        public static class Param {
            public static final String USER_ID = "userId";
        }

        public static class Name {
            public static final String FIND_CONVERSATIONS_BY_USER_ID = "findConvosByUserId";
            public static final String FIND_CONVERSATION_BY_USER_IDS = "findConvoByUserIds";
        }
    }
}
