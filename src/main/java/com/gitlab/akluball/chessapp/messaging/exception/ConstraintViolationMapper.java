package com.gitlab.akluball.chessapp.messaging.exception;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Payload;
import javax.validation.metadata.ConstraintDescriptor;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;
import java.util.Set;
import java.util.stream.Collectors;

@Provider
public class ConstraintViolationMapper implements ExceptionMapper<ConstraintViolationException> {
    @Context
    private Providers providers;

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        Set<Class<? extends Payload>> payloads = exception.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getConstraintDescriptor)
                .map(ConstraintDescriptor::getPayload)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
        if (payloads.contains(Http401.Payload.class)) {
            return this.providers.getExceptionMapper(Http401.class).toResponse(new Http401());
        } else {
            String reason = exception.getConstraintViolations()
                    .stream()
                    .map(ConstraintViolation::getMessage)
                    .collect(Collectors.joining(", "));
            return Response.status(400)
                    .entity(String.format("bad request: %s", reason))
                    .build();
        }
    }
}
