package com.gitlab.akluball.chessapp.messaging.security;

import java.util.HashMap;
import java.util.Map;

public enum BearerRole {
    USER("user"), SERVICE("service");

    public static final String CLAIM_NAME = "roles";
    private static final Map<String, BearerRole> STRING_TO_BEARER_ROLE = new HashMap<>();

    static {
        for (BearerRole bearerRole : BearerRole.values()) {
            STRING_TO_BEARER_ROLE.put(bearerRole.asString, bearerRole);
        }
    }

    private final String asString;

    BearerRole(String asString) {
        this.asString = asString;
    }

    public static BearerRole fromString(String stringRepr) {
        return STRING_TO_BEARER_ROLE.get(stringRepr);
    }

    public String asString() {
        return this.asString;
    }
}
