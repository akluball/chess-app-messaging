package com.gitlab.akluball.chessapp.messaging.exception;

public class Http403 extends HttpException {
    public Http403() {
        super(403, "not authorized");
    }
}
