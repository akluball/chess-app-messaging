package com.gitlab.akluball.chessapp.messaging.websocket;

import javax.websocket.EncodeException;
import javax.websocket.Session;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static com.gitlab.akluball.chessapp.messaging.util.Util.asRuntime;

public class WebSocketMesh {

    private final Map<Integer, Set<Session>> meshKeyToSessionsMap = new HashMap<>();

    public void register(int meshKey, Session session) {
        Set<Session> endpoints = this.meshKeyToSessionsMap.computeIfAbsent(meshKey, (key) -> new HashSet<>());
        endpoints.add(session);
    }

    public void broadcast(int meshKey, Object toBroadcast) {
        Set<Session> sessions = this.meshKeyToSessionsMap.get(meshKey);
        if (Objects.isNull(sessions)) {
            return;
        }
        sessions.forEach((session) -> {
            try {
                session.getBasicRemote().sendObject(toBroadcast);
            } catch (IOException | EncodeException e) {
                throw asRuntime(e);
            }
        });
    }

    public void unregister(int meshKey, Session session) {
        Set<Session> sessions = this.meshKeyToSessionsMap.get(meshKey);
        if (Objects.nonNull(session)) {
            sessions.remove(session);
        }
    }
}
