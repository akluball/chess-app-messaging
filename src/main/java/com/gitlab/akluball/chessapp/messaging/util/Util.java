package com.gitlab.akluball.chessapp.messaging.util;

import com.gitlab.akluball.chessapp.messaging.exception.HttpException;

import javax.ws.rs.core.Response;

public class Util {

    public static RuntimeException asRuntime(Throwable throwable) {
        return new RuntimeException(throwable);
    }

    public static HttpException getHttpExceptionCause(Throwable throwable) {
        while (throwable != null) {
            if (throwable instanceof HttpException) {
                return (HttpException) throwable;
            }
            throwable = throwable.getCause();
        }
        return null;
    }

    public static boolean is2xx(Response response) {
        return Integer.toString(response.getStatus()).startsWith("2");
    }

    public static int minutesToSeconds(int minutes) {
        return minutes * 60;
    }

    public static boolean isInteger(String toCheck) {
        try {
            Integer.parseInt(toCheck);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
