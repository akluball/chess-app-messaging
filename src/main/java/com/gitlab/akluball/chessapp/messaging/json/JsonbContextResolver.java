package com.gitlab.akluball.chessapp.messaging.json;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.bind.Jsonb;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class JsonbContextResolver implements ContextResolver<Jsonb> {
    @Inject
    private Jsonb jsonb;

    @Override
    public Jsonb getContext(Class<?> type) {
        return this.jsonb;
    }
}
