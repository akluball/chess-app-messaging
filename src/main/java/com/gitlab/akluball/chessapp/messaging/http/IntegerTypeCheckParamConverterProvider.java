package com.gitlab.akluball.chessapp.messaging.http;

import com.gitlab.akluball.chessapp.messaging.exception.Http400;

import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.gitlab.akluball.chessapp.messaging.util.Util.isInteger;

@Provider
public class IntegerTypeCheckParamConverterProvider implements ParamConverterProvider {
    private static final Set<Class<? extends Annotation>> PARAM_ANNOTATIONS = Stream.of(
            CookieParam.class,
            FormParam.class,
            HeaderParam.class,
            MatrixParam.class,
            PathParam.class,
            QueryParam.class
    ).collect(Collectors.toSet());

    @Context
    private ParamConverterProvider paramConverterProvider;

    @Override
    @SuppressWarnings("unchecked")
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        if (rawType.equals(Integer.class)) {
            ParamConverter<Integer> integerParamConverter = this.paramConverterProvider
                    .getConverter(Integer.class, genericType, annotations);
            return (ParamConverter<T>) new ParamConverter<Integer>() {
                @Override
                public Integer fromString(String value) {
                    if (isInteger(value)) {
                        return integerParamConverter.fromString(value);
                    } else if (Objects.isNull(value)) {
                        return null;
                    } else {
                        Annotation paramAnnotation = Stream.of(annotations)
                                .filter(annotation -> PARAM_ANNOTATIONS.contains(annotation.annotationType()))
                                .findFirst()
                                .orElseThrow(Http400::new);
                        String param = null;
                        String paramName = null;
                        if (paramAnnotation instanceof CookieParam) {
                            param = "cookie";
                            paramName = ((CookieParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof FormParam) {
                            param = "form";
                            paramName = ((FormParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof HeaderParam) {
                            param = "header";
                            paramName = ((HeaderParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof MatrixParam) {
                            param = "matrix";
                            paramName = ((MatrixParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof PathParam) {
                            param = "path";
                            paramName = ((PathParam) paramAnnotation).value();
                        } else if (paramAnnotation instanceof QueryParam) {
                            param = "query";
                            paramName = ((QueryParam) paramAnnotation).value();
                        }
                        if (Objects.nonNull(param)) {
                            throw new Http400(String.format("%s param %s must be an Integer", param, paramName));
                        } else {
                            throw new Http400();
                        }
                    }
                }

                @Override
                public String toString(Integer value) {
                    return integerParamConverter.toString(value);
                }
            };
        } else {
            return null;
        }
    }
}
