package com.gitlab.akluball.chessapp.messaging.transfer;

import com.gitlab.akluball.chessapp.messaging.data.ConverserDao;
import com.gitlab.akluball.chessapp.messaging.model.Converser;
import com.gitlab.akluball.chessapp.messaging.model.Message;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Schema(name = "ConversationInitializer")
public class ConversationInitializerDto {
    @NotNull(message = "ConversationInitializer.senderUserId is required")
    private Integer senderUserId;
    @NotEmpty(message = "ConversationInitializer.recipientUserIds must be non-empty")
    private Set<Integer> recipientUserIds = new HashSet<>();
    @NotBlank(message = "ConversationInitializer.messageText is required")
    private String messageText;

    public boolean isConverser(Integer userId) {
        return this.senderUserId.equals(userId);
    }

    public boolean isSenderRecipient() {
        return this.recipientUserIds.contains(this.senderUserId);
    }

    public Set<Converser> toConversers(ConverserDao converserDao) {
        return Stream.concat(this.recipientUserIds.stream(), Stream.of(this.senderUserId))
                .map(converserDao::findOrCreate)
                .collect(Collectors.toSet());
    }

    public Message toInitialMessage() {
        return new Message.Builder()
                .sender(this.senderUserId)
                .text(this.messageText)
                .build();
    }
}
