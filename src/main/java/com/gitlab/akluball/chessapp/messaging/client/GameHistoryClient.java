package com.gitlab.akluball.chessapp.messaging.client;

import com.gitlab.akluball.chessapp.messaging.MessagingConfig;
import com.gitlab.akluball.chessapp.messaging.model.GameConversation;
import com.gitlab.akluball.chessapp.messaging.security.MessagingSecurity;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static com.gitlab.akluball.chessapp.messaging.security.SecurityUtil.bearer;
import static com.gitlab.akluball.chessapp.messaging.util.Util.is2xx;

@Singleton
public class GameHistoryClient {

    private WebTarget target;
    private MessagingSecurity messagingSecurity;

    @Inject
    GameHistoryClient(MessagingConfig messagingConfig, Client client, MessagingSecurity messagingSecurity) {
        this.target = client.target(messagingConfig.gameHistoryUri())
                .path("api")
                .path("gamehistory");
        this.messagingSecurity = messagingSecurity;
    }

    public GameConversation.GameHistoryDto getGameHistory(int gameHistoryId) {
        Response response = this.target.path("" + gameHistoryId)
                .request()
                .header("Authorization", bearer(this.messagingSecurity.getToken()))
                .get();
        return is2xx(response) ? response.readEntity(GameConversation.GameHistoryDto.class) : null;
    }
}
