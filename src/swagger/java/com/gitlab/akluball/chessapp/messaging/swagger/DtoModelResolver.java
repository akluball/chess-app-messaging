package com.gitlab.akluball.chessapp.messaging.swagger;

import com.fasterxml.jackson.databind.JavaType;
import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.core.util.Json;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.media.Schema;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Objects;

public class DtoModelResolver implements ModelConverter {
    @Override
    public Schema resolve(AnnotatedType type, ModelConverterContext context, Iterator<ModelConverter> chain) {
        JavaType javaType = Json.mapper().constructType(type.getType());
        if (Objects.isNull(javaType) || !javaType.getRawClass().getName().endsWith("Dto")) {
            return chain.next().resolve(type, context, chain);
        }
        String modelName;
        io.swagger.v3.oas.annotations.media.Schema schemaAnnotation = javaType.getRawClass()
                .getAnnotation(io.swagger.v3.oas.annotations.media.Schema.class);
        if (Objects.nonNull(schemaAnnotation)) {
            modelName = schemaAnnotation.name();
        } else {
            modelName = javaType.getRawClass().getSimpleName();
        }
        Schema model = context.getDefinedModels().get(modelName);
        if (Objects.isNull(model)) {
            model = new ObjectSchema();
            for (Field field : javaType.getRawClass().getDeclaredFields()) {
                if (!field.getName().equals("this$0")) {
                    AnnotatedType annotatedType = new AnnotatedType()
                            .type(field.getGenericType())
                            .resolveAsRef(true);
                    Schema resolved = context.resolve(annotatedType);
                    model.addProperties(field.getName(), resolved);
                }
            }
            context.defineModel(modelName, model);
        }
        return new Schema().$ref(modelName);
    }
}
