package com.gitlab.akluball.chessapp.messaging.swagger;

import com.gitlab.akluball.chessapp.messaging.MessagingApplication;
import io.swagger.v3.jaxrs2.Reader;

import javax.ws.rs.ApplicationPath;
import java.util.Objects;

public class CustomReader extends Reader {
    @Override
    protected String resolveApplicationPath() {
        ApplicationPath applicationPathAnnotation = MessagingApplication.class.getAnnotation(ApplicationPath.class);
        if (Objects.nonNull(applicationPathAnnotation)) {
            return applicationPathAnnotation.value();
        } else {
            return super.resolveApplicationPath();
        }
    }
}
